import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:stocker/models/models.dart';
import 'package:stocker/services/api/api.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  group('Api Response', () {
    test('Standarized', () async {
      final json =
          await rootBundle.loadString('test/assets/product_api_response.json');

      final response = ApiResponse<Product>.fromJson(
        jsonDecode(json) as Map<String, dynamic>,
      );

      expect(response.success, true);
      expect(response.errors, null);
      expect(response.message, null);
      expect(response.payload, isA<Product>());
      expect(response.payload?.brand, isA<Brand>());
      expect(response.payload?.category, isA<Category>());
    });

    test('Paginated', () async {
      final json = await rootBundle
          .loadString('test/assets/brands_paginated_response.json');

      final response = ApiPaginatedResponse<Brand>.fromJson(
        jsonDecode(json) as Map<String, dynamic>,
      );

      expect(response.success, true);
      expect(response.message, null);
      expect(response.errors, null);
      expect(response.payload, isA<PaginatedResource>());
      expect(response.payload?.data, isA<List<Brand>>());
      expect(response.payload?.data.length, 5);
    });
    test('Form Validation Failed', () async {
      final json =
          await rootBundle.loadString('test/assets/auth_email_fail.json');

      final response = ApiPaginatedResponse<AuthResponse>.fromJson(
        jsonDecode(json) as Map<String, dynamic>,
      );

      expect(response.success, false);
      expect(response.payload, null);
      expect(response.message, "The given data was invalid");
      expect(response.errors, [
        const FieldException(
          'email',
          ["The email must be a valid email address."],
        )
      ]);
    });
  });

  group('Models', () {
    test('Brand', () async {
      final json = await rootBundle.loadString('test/assets/brand.json');
      final model = Brand.fromJson(jsonDecode(json) as Map<String, dynamic>);

      final expected = Brand(
        id: 2,
        name: 'Zuxx',
        description: 'Quality Milk Provider',
        createdAt: DateTime.utc(2021, 7, 3, 21, 38, 47),
        updatedAt: DateTime.utc(2021, 7, 3, 21, 38, 47),
      );

      expect(model, expected);
    });

    test('Product', () async {
      final json = await rootBundle.loadString('test/assets/product.json');
      final model = Product.fromJson(jsonDecode(json) as Map<String, dynamic>);

      expect(model, isA<Product>());
    });
  });
}
