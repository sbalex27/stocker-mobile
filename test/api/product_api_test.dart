import 'package:faker/faker.dart';
import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:stocker/configurations/constrants.dart';
import 'package:stocker/extensions/faker.dart';
import 'package:stocker/models/models.dart';
import 'package:stocker/services/api/api.dart';
import 'product_api_test.mocks.dart';

@GenerateMocks([StockerClient])
void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  test('index', () async {
    final client = MockStockerClient();
    const data = 'assets/models/products/index.json';
    final uri = Uri.http(kWSDomain, 'api/products', {'page': '1'});

    when(client.get(uri)).thenAnswer((_) async {
      return Response(await rootBundle.loadString(data), 200);
    });

    expect(
      await ProductsApi(client).index(1),
      isA<PaginatedResource<Product>>(),
    );
  });

  test('show', () async {
    final client = MockStockerClient();
    const data = 'assets/models/products/show.json';
    final uri = Uri.http(kWSDomain, 'api/products/1', {'detailed': 'false'});

    when(client.get(uri)).thenAnswer((_) async {
      return Response(await rootBundle.loadString(data), 200);
    });

    expect(await ProductsApi(client).show(1), isA<Product>());
  });

  test('show detailed', () async {
    final client = MockStockerClient();
    const data = 'assets/models/products/show_detailed.json';
    final url = Uri.http(kWSDomain, 'api/products/1', {'detailed': 'true'});

    when(client.get(url)).thenAnswer((_) async {
      return Response(await rootBundle.loadString(data), 200);
    });

    expect(await ProductsApi(client).show(1, detailed: true), isA<Product>());
  });

  test('update', () async {
    final client = MockStockerClient();
    const data = 'assets/models/products/update.json';
    final url = Uri.http(kWSDomain, 'api/products/1');
    final model = Faker().product(1);

    when(client.put(url, body: model.toJson())).thenAnswer((_) async {
      return Response(await rootBundle.loadString(data), 200);
    });

    expect(await ProductsApi(client).update(1, model), isA<Product>());
  });

  test('delete', () async {
    final client = MockStockerClient();
    const data = 'assets/models/products/delete.json';
    final url = Uri.http(kWSDomain, 'api/products/1');
    final model = Faker().product(1);

    when(client.delete(url)).thenAnswer((realInvocation) async {
      return Response(await rootBundle.loadString(data), 200);
    });

    expect(await ProductsApi(client).delete(model), isA<Product>());
  });
}
