// Mocks generated by Mockito 5.0.15 from annotations
// in stocker/test/products_test.dart.
// Do not manually edit this file.

import 'dart:async' as _i5;

import 'package:mockito/mockito.dart' as _i1;
import 'package:stocker/models/models.dart' as _i3;
import 'package:stocker/repositories/repositories.dart' as _i4;
import 'package:stocker/services/api/api.dart' as _i2;

// ignore_for_file: avoid_redundant_argument_values
// ignore_for_file: avoid_setters_without_getters
// ignore_for_file: comment_references
// ignore_for_file: implementation_imports
// ignore_for_file: invalid_use_of_visible_for_testing_member
// ignore_for_file: prefer_const_constructors
// ignore_for_file: unnecessary_parenthesis

class _FakeResourceRouter_0 extends _i1.Fake implements _i2.ResourceRouter {}

class _FakeStockerClient_1 extends _i1.Fake implements _i2.StockerClient {}

class _FakeProduct_2 extends _i1.Fake implements _i3.Product {}

class _FakePaginated_3<T> extends _i1.Fake implements _i4.Paginated<T> {}

/// A class which mocks [ProductsApi].
///
/// See the documentation for Mockito's code generation for more information.
class MockProductsApi extends _i1.Mock implements _i2.ProductsApi {
  MockProductsApi() {
    _i1.throwOnMissingStub(this);
  }

  @override
  _i2.ResourceRouter get router =>
      (super.noSuchMethod(Invocation.getter(#router),
          returnValue: _FakeResourceRouter_0()) as _i2.ResourceRouter);
  @override
  _i2.StockerClient get client =>
      (super.noSuchMethod(Invocation.getter(#client),
          returnValue: _FakeStockerClient_1()) as _i2.StockerClient);
  @override
  _i5.Future<_i3.Product> show(int? id, {bool? detailed = false}) =>
      (super.noSuchMethod(Invocation.method(#show, [id], {#detailed: detailed}),
              returnValue: Future<_i3.Product>.value(_FakeProduct_2()))
          as _i5.Future<_i3.Product>);
  @override
  String toString() => super.toString();
  @override
  _i5.Future<_i3.Product> store(_i3.Serializable? model) =>
      (super.noSuchMethod(Invocation.method(#store, [model]),
              returnValue: Future<_i3.Product>.value(_FakeProduct_2()))
          as _i5.Future<_i3.Product>);
  @override
  _i5.Future<_i3.Product> update(int? id, _i3.Serializable? model) =>
      (super.noSuchMethod(Invocation.method(#update, [id, model]),
              returnValue: Future<_i3.Product>.value(_FakeProduct_2()))
          as _i5.Future<_i3.Product>);
  @override
  _i5.Future<_i3.Product> delete(_i3.Identifiable? model) =>
      (super.noSuchMethod(Invocation.method(#delete, [model]),
              returnValue: Future<_i3.Product>.value(_FakeProduct_2()))
          as _i5.Future<_i3.Product>);
  @override
  _i5.Future<_i4.Paginated<_i3.Product>> index(int? page) =>
      (super.noSuchMethod(Invocation.method(#index, [page]),
              returnValue: Future<_i4.Paginated<_i3.Product>>.value(
                  _FakePaginated_3<_i3.Product>()))
          as _i5.Future<_i4.Paginated<_i3.Product>>);
}
