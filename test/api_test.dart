import 'dart:convert';

import 'package:faker/faker.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:stocker/configurations/constrants.dart';
import 'package:stocker/extensions/faker.dart';
import 'package:stocker/repositories/repositories.dart';
import 'package:stocker/services/api/api.dart';

import 'api_test.mocks.dart';

@GenerateMocks([StockerClient])
void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  test('Standarized Response', () async {
    // Assignation
    final client = MockStockerClient();
    final faker = Faker();
    final response = ApiResponse(
      success: true,
      payload: faker.product(1),
    );

    // Mocking
    when(
      client.get(Uri.http(kWSDomain, 'api/products/1', {'detailed': 'false'})),
    ).thenAnswer(
      (_) async => http.Response(jsonEncode(response.toJson()), 200),
    );

    // Response
    expect(await ProductsApi(client).show(1), response.payload);
  });

  test('Paginated Response', () async {
    // Assignation
    final client = MockStockerClient();
    final faker = Faker();
    final data = List.generate(20, (index) => faker.brand(index));
    final paginated = faker.paginatedResource(data: data);
    final response = ApiPaginatedResponse(
      success: true,
      payload: paginated,
    );

    // Mocking
    when(client.get(Uri.http(kWSDomain, 'api/brands', {'page': '1'})))
        .thenAnswer(
      (realInvocation) async {
        return http.Response(jsonEncode(response.toJson()), 200);
      },
    );

    final result = await BrandsApi(client).index(1);

    // Assert
    expect(result, isA<Paginated>());
    expect(result.data, data.getRange(0, 10).toList());
  });

  test('Unsuccessful Response', () {
    // Assignation
    final client = MockStockerClient();
    final api = ProductsApi(client);

    when(
      client.get(Uri.http(kWSDomain, 'api/products/1', {'detailed': 'false'})),
    ).thenAnswer(
      (realInvocation) async =>
          http.Response('', 500, reasonPhrase: "Internal server error"),
    );

    expect(
      () async {
        return api.show(1);
      },
      throwsA(
        isA<RepositoryException>(),
      ),
    );
  });
}
