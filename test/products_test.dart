import 'package:faker/faker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';
import 'package:stocker/extensions/faker.dart';
import 'package:stocker/main.dart';
import 'package:stocker/repositories/repositories.dart';
import 'package:stocker/services/api/api.dart';
import 'package:stocker/views/products/products_screen.dart';
import 'package:stocker/widgets/providers/authentication.dart';

import 'products_test.mocks.dart';

@GenerateMocks([ProductsApi])
void main() {
  final faker = Faker();

  group('repository', () {
    test('index', () async {
      final repository = MockProductsApi();
      final data = List.generate(50, (index) => faker.product(index));
      final pagination = faker.paginatedResource(data: data);

      when(repository.index(1))
          .thenAnswer((realInvocation) async => pagination);

      expect(
        await repository.index(1).then((value) => value.data),
        data.getRange(0, 10).toList(),
      );
    });

    test('show', () async {
      final repository = MockProductsApi();

      final model = faker.product(1);
      when(repository.show(1)).thenAnswer((realInvocation) async => model);
      expect(await repository.show(1), model);
    });

    test('create', () async {
      final repository = MockProductsApi();

      final model = faker.product(null);
      final expected = model..id = 20;

      when(repository.store(model))
          .thenAnswer((realInvocation) async => expected);

      final resolved = await repository.store(model);

      expect(resolved, expected);
    });

    test('update', () async {
      final repository = MockProductsApi();

      final model = faker.product(20);
      final expected = model
        ..name = faker.lorem.word()
        ..updatedAt = DateTime.now();

      when(repository.update(model.id, model))
          .thenAnswer((realInvocation) async => expected);

      final resolved = await repository.update(model.id, model);

      expect(resolved, expected);
    });

    test('delete', () async {
      final repository = MockProductsApi();

      final model = faker.product(30);
      final expected = model..deletedAt = DateTime.now();

      when(repository.delete(model))
          .thenAnswer((realInvocation) async => expected);

      final resolved = await repository.delete(model);

      expect(resolved, expected);
    });
  });

  group('widgets', () {
    group('details screen', () {
      final repository = MockProductsApi();
      final model = faker.product(1);
      when(repository.show(1)).thenAnswer((realInvocation) async {
        return Future.delayed(const Duration(seconds: 1), () => model);
      });

      testWidgets('without initial data', (tester) async {
        await tester.pumpWidget(
          StockerApp(
            providers: [
              ChangeNotifierProvider(
                create: (context) => Authentication(
                  token: faker.jwt.valid(),
                  user: faker.user(1),
                ),
              ),
              Provider<ProductRepository>.value(
                value: repository,
              )
            ],
            initialRoute: '/products/1',
          ),
        );

        await tester.pump();

        expect(find.byType(CircularProgressIndicator), findsOneWidget);

        await tester.pumpAndSettle();

        expect(find.text(model.name), findsOneWidget);
      });

      testWidgets('with initial data', (tester) async {
        await tester.pumpWidget(
          StockerApp(
            providers: [
              ChangeNotifierProvider(
                create: (_) => Authentication(
                  user: faker.user(1),
                  token: faker.jwt.valid(),
                ),
              ),
              Provider<ProductRepository>.value(
                value: repository,
              )
            ],
            initialRoute: '/products/1',
          ),
        );

        await tester.pump();

        expect(find.byType(CircularProgressIndicator), findsNothing);
        expect(find.text(model.name), findsOneWidget);

        await tester.pump(const Duration(seconds: 1));
      });
    });

    group('index screen', () {
      testWidgets('success', (tester) async {
        // Mocking
        final repository = MockProductsApi();
        final faker = Faker();
        final data = List.generate(90, (index) => faker.product(index + 1));
        when(repository.index(any)).thenAnswer((realInvocation) async {
          return Future.delayed(const Duration(seconds: 1), () {
            return faker.paginatedResource(
              data: data,
              page: realInvocation.positionalArguments[0] as int,
              perPage: 20,
            );
          });
        });

        // Building
        await tester.pumpWidget(
          MaterialApp(
            home: Provider<ProductRepository>.value(
              value: repository,
              child: const Scaffold(
                body: ProductsScreen(),
              ),
            ),
          ),
        );

        // Matchers
        final pagedListView = find.byKey(const Key('products_list'));
        final progressIndicator = find.byType(CircularProgressIndicator);
        final tiles = find.byType(ProductTile);

        expect(progressIndicator, findsOneWidget);
        expect(tiles, findsNothing);

        await tester.pumpAndSettle();

        expect(progressIndicator, findsNothing);

        await tester.dragUntilVisible(
          find.text(data[9].name),
          pagedListView,
          const Offset(0.0, -500.00),
        );

        expect(find.text(data[9].name), findsOneWidget);
      });
    });
  });
}
