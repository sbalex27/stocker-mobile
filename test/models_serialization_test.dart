import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:stocker/models/models.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  test('Product', () async {
    final product = await rootBundle
        .loadString('test/assets/show_product.json')
        .then((source) => jsonDecode(source))
        .then((value) => value['payload'] as Map<String, dynamic>)
        .then((json) => Product.fromJson(json));

    expect(product, isA<Product>());
  });

  test('Brand', () async {
    final brand = await rootBundle
        .loadString('test/assets/brand.json')
        .then((source) => jsonDecode(source))
        .then((value) => value as Map<String, dynamic>)
        .then((json) => Brand.fromJson(json));

    expect(brand, isA<Brand>());
  });

  test('Category Copy', () {
    final a = Category(
      id: 1,
      name: 'Cups',
      createdAt: DateTime.now(),
      updatedAt: DateTime.now(),
    );

    final b = Category(
      id: 1,
      name: 'Cups',
      createdAt: DateTime.now(),
      updatedAt: DateTime.now(),
    );

    expect(a, b);
  });
}
