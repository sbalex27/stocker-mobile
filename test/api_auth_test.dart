import 'dart:convert';

import 'package:faker/faker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';
import 'package:stocker/configurations/constrants.dart';
import 'package:stocker/extensions/faker.dart';
import 'package:stocker/lang/l10n.dart';
import 'package:stocker/models/models.dart';
import 'package:stocker/repositories/repositories.dart';
import 'package:stocker/services/api/api.dart';
import 'package:stocker/views/login/login_screen.dart';
import 'package:stocker/widgets/providers/authentication.dart';

import 'api_auth_test.mocks.dart';
import 'api_test.mocks.dart';

@GenerateMocks([AuthApi])
void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  group('api', () {
    group('login', () {
      final client = MockStockerClient();
      final api = AuthApi(client);
      final url = Uri.http(kWSDomain, 'api/auth/login');
      final credentials = Auth(
        email: 'test@gmail.com',
        password: '12345678',
      );
      test('success', () async {
        // Assignation
        final expected = await rootBundle
            .loadString('test/assets/login.json')
            .then((value) => jsonDecode(value))
            .then((value) => value as Map<String, dynamic>);

        final payload = expected['payload'] as Map;
        final token = payload['token'];
        final auth = User.fromJson(payload['user'] as Map<String, dynamic>);

        // Mocking
        when(client.post(url, body: anyNamed('body'))).thenAnswer((_) async {
          return http.Response(jsonEncode(expected), 200);
        });

        // Perform
        final response = await api.login(credentials);

        //Assert
        expect(response.token, token);
        expect(response.user, auth);
      });

      test('unauthorized', () async {
        // Assignation
        final expected = await rootBundle
            .loadString('test/assets/unauthorized_response.json')
            .then((value) => jsonDecode(value))
            .then((value) => value as Map<String, dynamic>);

        // Mocking
        when(client.post(url, body: anyNamed('body'))).thenAnswer((_) async {
          return http.Response(jsonEncode(expected), 401);
        });

        // Perform and Assert
        expect(
          () async {
            await api.login(credentials);
          },
          throwsA(isA<UnauthorizedException>()),
        );
      });

      test('form exception', () async {
        // Assignation
        final expected = await rootBundle
            .loadString('test/assets/auth_email_fail.json')
            .then((value) => jsonDecode(value))
            .then((value) => value as Map<String, dynamic>);

        // Mocking
        when(client.post(url, body: anyNamed('body'))).thenAnswer((_) async {
          return http.Response(jsonEncode(expected), 422);
        });

        // Perform and Assert
        expect(
          () async {
            await api.login(credentials);
          },
          throwsA(isA<UnsuccessfulException>()),
        );
      });
    });

    test('logged', () async {
      final client = MockStockerClient();
      final url = Uri.http(kWSDomain, '/api/auth/logged');
      final expected = await rootBundle
          .loadString('test/assets/logged.json')
          .then((value) => jsonDecode(value))
          .then((value) => value as Map<String, dynamic>);

      // Mocking
      when(client.get(url)).thenAnswer((_) async {
        return http.Response(jsonEncode(expected), 200);
      });

      // Perform
      final response = await AuthApi(client).logged();

      // Assert
      expect(response, isA<User>());
    });

    group('logout', () {
      final client = MockStockerClient();
      final api = AuthApi(client);
      final url = Uri.http(kWSDomain, '/api/auth/logout');

      test('success', () async {
        final expected = await rootBundle
            .loadString('test/assets/logout.json')
            .then((value) => jsonDecode(value))
            .then((value) => value as Map<String, dynamic>);

        // Mocking
        when(client.post(url)).thenAnswer((_) async {
          return http.Response(jsonEncode(expected), 200);
        });

        // Assert
        final response = await api.logout();
        expect(response, isA<User>());
      });

      test('unauthorized', () async {
        final expected = await rootBundle
            .loadString('test/assets/unauthorized_response.json')
            .then((value) => jsonDecode(value))
            .then((value) => value as Map<String, dynamic>);

        when(client.post(url)).thenAnswer((_) async {
          return http.Response(jsonEncode(expected), 401);
        });

        expect(
          () async {
            await api.logout();
          },
          throwsA(isA<UnauthorizedException>()),
        );
      });
    });
  });

  group('widgets', () {
    group('login screen', () {
      final faker = Faker(seed: 1);
      final lang = Language();

      testWidgets('success auth', (tester) async {
        // Mocking
        final api = MockAuthApi();
        when(api.login(any)).thenAnswer((_) {
          return Future.delayed(const Duration(seconds: 1), () {
            return AuthResponse(
              user: faker.user(1),
              token: faker.jwt.toString(),
            );
          });
        });

        // Perform
        await tester.pumpWidget(_TestableLoginScreen(api: api));
        await tester.pump();

        // Fill Form
        final inputFields = find.byType(TextFormField);
        expect(inputFields, findsNWidgets(2));
        await tester.enterText(inputFields.at(0), "user@example.com");
        await tester.enterText(inputFields.at(1), "12345678");

        // Perform Request
        await tester.tap(find.byType(ElevatedButton));
        await tester.pump();
        expect(find.byType(CircularProgressIndicator), findsOneWidget);

        await tester.pump(const Duration(seconds: 1));
      });

      testWidgets('client invalid form', (tester) async {
        // Mocking & data
        final api = MockAuthApi();

        // Render
        await tester.pumpWidget(_TestableLoginScreen(api: api));
        await tester.pump();

        // Tap
        await tester.tap(find.byType(ElevatedButton));
        await tester.pump();

        // Expect
        final requiredEmail = lang.requiredField(lang.email.toLowerCase());
        final requiredPassword =
            lang.requiredField(lang.password.toLowerCase());

        expect(find.text(requiredEmail), findsOneWidget);
        expect(find.text(requiredPassword), findsOneWidget);
      });

      testWidgets('server invalid form', (tester) async {
        // Mocking
        final api = MockAuthApi();
        final exception = UnsuccessfulException(
          message: lang.errorTitle,
          errors: [
            const FieldException("email", ["Invalid Email"])
          ],
        );
        when(api.login(any)).thenAnswer((_) {
          return Future.delayed(
            const Duration(seconds: 1),
            () => Future.error(exception),
          );
        });

        // Render
        await tester.pumpWidget(_TestableLoginScreen(api: api));
        await tester.pump();

        // Fill
        final fields = find.byType(TextFormField);
        await tester.enterText(fields.at(0), "example@gmail.com");
        await tester.enterText(fields.at(1), "12345678");

        // Send Request
        await tester.tap(find.byType(ElevatedButton));
        await tester.pump(const Duration(seconds: 1));
        expect(find.text(exception.errors.first.errors.first), findsOneWidget);
        expect(find.text(exception.message), findsOneWidget);
      });
      testWidgets('any exception', (tester) async {
        // Mocking & Data
        final api = MockAuthApi();
        when(api.login(any)).thenAnswer((_) {
          return Future.delayed(
            const Duration(seconds: 1),
            () => Future.error(Exception("Any Exception")),
          );
        });

        // Render
        await tester.pumpWidget(_TestableLoginScreen(api: api));
        await tester.pump();

        // Fill & tap
        final fields = find.byType(TextFormField);
        await tester.enterText(fields.first, faker.internet.email());
        await tester.enterText(fields.last, faker.internet.password());
        await tester.tap(find.byType(ElevatedButton));
        await tester.pump(const Duration(seconds: 1));

        // Assert
        expect(find.text(lang.errorTitle), findsOneWidget);
        expect(find.text(lang.errorCaptionRetry), findsOneWidget);
      });
    });
  });
}

class _TestableLoginScreen extends StatelessWidget {
  const _TestableLoginScreen({Key? key, required this.api}) : super(key: key);

  final AuthApi api;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MultiProvider(
        providers: [
          Provider<AuthApi>.value(value: api),
          ChangeNotifierProvider(
            create: (_) => Authentication(),
          )
        ],
        child: const LoginScreen(),
      ),
      localizationsDelegates: const [Language.delegate],
    );
  }
}
