// import 'package:faker/faker.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_test/flutter_test.dart';
// import 'package:provider/provider.dart';
// import 'package:stocker/extensions/faker.dart';
// import 'package:stocker/main.dart';
// import 'package:stocker/models/models.dart';
// import 'package:stocker/repositories/repositories.dart';
// import 'package:stocker/views/home/home_screen.dart';
// import 'package:stocker/views/login/login_screen.dart';
// import 'package:stocker/views/product_details/product_details_screen.dart';
// import 'package:stocker/views/products/products_screen.dart';
// import 'package:stocker/views/profile/profile_screen.dart';
// import 'package:stocker/widgets/providers/authentication.dart';

// void main() {
//   testWidgets('home', (tester) async {
//     await tester.pumpWidget(_Builder(initialRoute: '/'));
//     await tester.pumpAndSettle();
//     expect(find.byType(HomeScreen), findsOneWidget);
//   });

//   testWidgets('profile', (tester) async {
//     await tester.pumpWidget(_Builder(initialRoute: '/profile'));
//     await tester.pumpAndSettle();
//     expect(find.byType(ProfileScreen), findsOneWidget);
//   });

//   testWidgets('login', (tester) async {
//     await tester.pumpWidget(
//       StockerApp(
//         providers: [
//           ChangeNotifierProvider(
//             create: (context) => Authentication(),
//           )
//         ],
//         initialRoute: '/',
//       ),
//     );
//     await tester.pumpAndSettle();
//     expect(find.byType(LoginScreen), findsOneWidget);
//   });

//   group('products', () {
//     testWidgets('index', (tester) async {
//       await tester.pumpWidget(_Builder(initialRoute: '/products'));
//       await tester.pumpAndSettle();
//       expect(find.byType(ProductsScreen), findsOneWidget);
//     });

//     testWidgets('show', (tester) async {
//       await tester.pumpWidget(_Builder(initialRoute: '/products/1'));
//       await tester.pumpAndSettle();
//       expect(find.byType(ProductDetailsScreen), findsOneWidget);
//     });
//   });
// }

// class _Builder extends StatelessWidget {
//   _Builder({
//     Key? key,
//     required this.initialRoute,
//   }) : super(key: key);

//   final faker = Faker(seed: 1);
//   final String initialRoute;

//   List<Product> get products =>
//       List.generate(10, (index) => faker.product(index + 1));

//   @override
//   Widget build(BuildContext context) {
//     return StockerApp(
//       providers: [
//         ChangeNotifierProvider(
//           create: (_) => Authentication(
//             token: faker.jwt.valid(),
//             user: faker.user(1),
//           ),
//         ),
//         Provider<ProductRepository>(
//           create: (context) => ProductsApiMock(
//             data: products,
//             faker: faker,
//           ),
//         )
//       ],
//       initialRoute: initialRoute,
//     );
//   }
// }
