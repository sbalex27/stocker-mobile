import 'package:flutter_test/flutter_test.dart';
import 'package:stocker/configurations/constrants.dart';
import 'package:stocker/services/api/api.dart';

void main() {
  test('Sliced route model binding', () {
    final siliced = const SlicedPath('products', '2').toString();
    expect(siliced, '/products/2');
  });

  test('Sliced route', () {
    final actual = const SlicedPath('products').toString();
    expect(actual, '/products');
  });

  test('Single model binded resource routing', () {
    final actual = ResourceRouter(
      parts: ['products'],
    ).resolve(['1']).toString();

    final matcher = Uri.http(kWSDomain, 'api/products/1').toString();

    expect(actual, matcher);
  });

  test('Single resource routing', () {
    final actual = ResourceRouter(parts: ['products']).resolve();

    final matcher = Uri.http(kWSDomain, 'api/products');

    expect(actual, matcher);
  });

  test('Double model binded resource routing', () {
    final actual = ResourceRouter(
      parts: ['branch-offices', 'products'],
    ).resolve(['5', '10']).toString();

    final matcher =
        Uri.http(kWSDomain, 'api/branch-offices/5/products/10').toString();

    expect(actual, matcher);
  });

  test('Double resource routing', () {
    final actual = ResourceRouter(
      parts: ['contacts', 'references'],
    ).resolve(['200']).toString();

    final matcher =
        Uri.http(kWSDomain, 'api/contacts/200/references').toString();

    expect(actual, matcher);
  });
}
