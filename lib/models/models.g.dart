// GENERATED CODE - DO NOT MODIFY BY HAND

part of models;

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Map<String, dynamic> _$AuthToJson(Auth instance) => <String, dynamic>{
      'email': instance.email,
      'password': instance.password,
    };

Product _$ProductFromJson(Map<String, dynamic> json) {
  return Product(
    id: json['id'] as int?,
    brandId: json['brand_id'] as int,
    categoryId: json['category_id'] as int,
    name: json['name'] as String,
    value: (json['value'] as num).toDouble(),
    gain: (json['gain'] as num).toDouble(),
    discountable: json['discountable'] as bool,
    createdAt: DateTime.parse(json['created_at'] as String),
    updatedAt: DateTime.parse(json['updated_at'] as String),
    sales: (json['sales'] as List<dynamic>?)
        ?.map((e) => SaleVoucher.fromJson(e as Map<String, dynamic>))
        .toList(),
    purchases: (json['purchases'] as List<dynamic>?)
        ?.map((e) => PurchaseOrder.fromJson(e as Map<String, dynamic>))
        .toList(),
    branchOffices: (json['branch_offices'] as List<dynamic>?)
        ?.map((e) => BranchOffice.fromJson(e as Map<String, dynamic>))
        .toList(),
    deletedAt: json['deleted_at'] == null
        ? null
        : DateTime.parse(json['deleted_at'] as String),
    brand: json['brand'] == null
        ? null
        : Brand.fromJson(json['brand'] as Map<String, dynamic>),
    category: json['category'] == null
        ? null
        : Category.fromJson(json['category'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$ProductToJson(Product instance) => <String, dynamic>{
      'brand_id': instance.brandId,
      'category_id': instance.categoryId,
      'name': instance.name,
      'value': instance.value,
      'gain': instance.gain,
      'discountable': instance.discountable,
      'brand': instance.brand,
      'category': instance.category,
      'branch_offices': instance.branchOffices,
      'sales': instance.sales,
      'purchases': instance.purchases,
      'created_at': instance.createdAt.toIso8601String(),
      'deleted_at': instance.deletedAt?.toIso8601String(),
      'id': instance.id,
      'updated_at': instance.updatedAt.toIso8601String(),
    };

Brand _$BrandFromJson(Map<String, dynamic> json) {
  return Brand(
    id: json['id'] as int?,
    name: json['name'] as String,
    createdAt: DateTime.parse(json['created_at'] as String),
    updatedAt: DateTime.parse(json['updated_at'] as String),
    deletedAt: json['deleted_at'] == null
        ? null
        : DateTime.parse(json['deleted_at'] as String),
    description: json['description'] as String?,
  );
}

Map<String, dynamic> _$BrandToJson(Brand instance) => <String, dynamic>{
      'name': instance.name,
      'description': instance.description,
      'created_at': instance.createdAt.toIso8601String(),
      'deleted_at': instance.deletedAt?.toIso8601String(),
      'id': instance.id,
      'updated_at': instance.updatedAt.toIso8601String(),
    };

Category _$CategoryFromJson(Map<String, dynamic> json) {
  return Category(
    id: json['id'] as int?,
    name: json['name'] as String,
    createdAt: DateTime.parse(json['created_at'] as String),
    updatedAt: DateTime.parse(json['updated_at'] as String),
    deletedAt: json['deleted_at'] == null
        ? null
        : DateTime.parse(json['deleted_at'] as String),
    description: json['description'] as String?,
  );
}

Map<String, dynamic> _$CategoryToJson(Category instance) => <String, dynamic>{
      'name': instance.name,
      'description': instance.description,
      'created_at': instance.createdAt.toIso8601String(),
      'deleted_at': instance.deletedAt?.toIso8601String(),
      'id': instance.id,
      'updated_at': instance.updatedAt.toIso8601String(),
    };

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    id: json['id'] as int?,
    firstname: json['firstname'] as String,
    email: json['email'] as String,
    role: _$enumDecode(_$UserRoleEnumMap, json['role']),
    createdAt: DateTime.parse(json['created_at'] as String),
    updatedAt: DateTime.parse(json['updated_at'] as String),
    deletedAt: json['deleted_at'] == null
        ? null
        : DateTime.parse(json['deleted_at'] as String),
    lastname: json['lastname'] as String?,
  );
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'firstname': instance.firstname,
      'lastname': instance.lastname,
      'email': instance.email,
      'role': _$UserRoleEnumMap[instance.role],
      'created_at': instance.createdAt.toIso8601String(),
      'deleted_at': instance.deletedAt?.toIso8601String(),
      'id': instance.id,
      'updated_at': instance.updatedAt.toIso8601String(),
    };

K _$enumDecode<K, V>(
  Map<K, V> enumValues,
  Object? source, {
  K? unknownValue,
}) {
  if (source == null) {
    throw ArgumentError(
      'A value must be provided. Supported values: '
      '${enumValues.values.join(', ')}',
    );
  }

  return enumValues.entries.singleWhere(
    (e) => e.value == source,
    orElse: () {
      if (unknownValue == null) {
        throw ArgumentError(
          '`$source` is not one of the supported values: '
          '${enumValues.values.join(', ')}',
        );
      }
      return MapEntry(unknownValue, enumValues.values.first);
    },
  ).key;
}

const _$UserRoleEnumMap = {
  UserRole.admin: 'admin',
  UserRole.cashier: 'cashier',
};

PurchaseOrder _$PurchaseOrderFromJson(Map<String, dynamic> json) {
  return PurchaseOrder(
    contactId: json['contact_id'] as int,
    branchOfficeId: json['branch_office_id'] as int,
    byUserId: json['by_user_id'] as int,
    status: _$enumDecode(_$PurchaseOrderStatusEnumMap, json['status']),
    createdAt: DateTime.parse(json['created_at'] as String),
    updatedAt: DateTime.parse(json['updated_at'] as String),
    branchOffice: json['branch_office'] == null
        ? null
        : BranchOffice.fromJson(json['branch_office'] as Map<String, dynamic>),
    contact: json['contact'] == null
        ? null
        : Contact.fromJson(json['contact'] as Map<String, dynamic>),
    productPurchase: json['product_purchase'] == null
        ? null
        : ProductPurchase.fromJson(
            json['product_purchase'] as Map<String, dynamic>),
    byUser: json['by_user'] == null
        ? null
        : User.fromJson(json['by_user'] as Map<String, dynamic>),
    dueDate: json['due_date'] == null
        ? null
        : DateTime.parse(json['due_date'] as String),
    deletedAt: json['deleted_at'] == null
        ? null
        : DateTime.parse(json['deleted_at'] as String),
    id: json['id'] as int?,
  );
}

Map<String, dynamic> _$PurchaseOrderToJson(PurchaseOrder instance) =>
    <String, dynamic>{
      'contact_id': instance.contactId,
      'branch_office_id': instance.branchOfficeId,
      'by_user_id': instance.byUserId,
      'by_user': instance.byUser,
      'status': _$PurchaseOrderStatusEnumMap[instance.status],
      'due_date': instance.dueDate?.toIso8601String(),
      'product_purchase': instance.productPurchase,
      'contact': instance.contact,
      'branch_office': instance.branchOffice,
      'created_at': instance.createdAt.toIso8601String(),
      'deleted_at': instance.deletedAt?.toIso8601String(),
      'id': instance.id,
      'updated_at': instance.updatedAt.toIso8601String(),
    };

const _$PurchaseOrderStatusEnumMap = {
  PurchaseOrderStatus.undelivered: 'undelivered',
  PurchaseOrderStatus.delivered: 'delivered',
};

SaleVoucher _$SaleVoucherFromJson(Map<String, dynamic> json) {
  return SaleVoucher(
    contactId: json['contact_id'] as int,
    paymentType:
        _$enumDecode(_$SaleVoucherPaymentTypeEnumMap, json['payment_type']),
    createdAt: DateTime.parse(json['created_at'] as String),
    updatedAt: DateTime.parse(json['updated_at'] as String),
    branchOfficeId: json['branch_office_id'] as int,
    branchOffice: json['branch_office'] == null
        ? null
        : BranchOffice.fromJson(json['branch_office'] as Map<String, dynamic>),
    productSale: json['product_sale'] == null
        ? null
        : ProductSale.fromJson(json['product_sale'] as Map<String, dynamic>),
    discountId: json['discount_id'] as int?,
    dueDate: json['due_date'] == null
        ? null
        : DateTime.parse(json['due_date'] as String),
    deletedAt: json['deleted_at'] == null
        ? null
        : DateTime.parse(json['deleted_at'] as String),
    id: json['id'] as int?,
  )..contact = json['contact'] == null
      ? null
      : Contact.fromJson(json['contact'] as Map<String, dynamic>);
}

Map<String, dynamic> _$SaleVoucherToJson(SaleVoucher instance) =>
    <String, dynamic>{
      'contact_id': instance.contactId,
      'discount_id': instance.discountId,
      'payment_type': _$SaleVoucherPaymentTypeEnumMap[instance.paymentType],
      'due_date': instance.dueDate?.toIso8601String(),
      'product_sale': instance.productSale,
      'contact': instance.contact,
      'branch_office_id': instance.branchOfficeId,
      'branch_office': instance.branchOffice,
      'created_at': instance.createdAt.toIso8601String(),
      'deleted_at': instance.deletedAt?.toIso8601String(),
      'id': instance.id,
      'updated_at': instance.updatedAt.toIso8601String(),
    };

const _$SaleVoucherPaymentTypeEnumMap = {
  SaleVoucherPaymentType.cash: 'cash',
  SaleVoucherPaymentType.credit: 'credit',
};

BranchOffice _$BranchOfficeFromJson(Map<String, dynamic> json) {
  return BranchOffice(
    name: json['name'] as String,
    adress: json['adress'] as String,
    phone: json['phone'] as String,
    createdAt: DateTime.parse(json['created_at'] as String),
    updatedAt: DateTime.parse(json['updated_at'] as String),
    stock: json['stock'] == null
        ? null
        : Stock.fromJson(json['stock'] as Map<String, dynamic>),
    deletedAt: json['deleted_at'] == null
        ? null
        : DateTime.parse(json['deleted_at'] as String),
    id: json['id'] as int?,
  );
}

Map<String, dynamic> _$BranchOfficeToJson(BranchOffice instance) =>
    <String, dynamic>{
      'name': instance.name,
      'adress': instance.adress,
      'phone': instance.phone,
      'stock': instance.stock,
      'created_at': instance.createdAt.toIso8601String(),
      'deleted_at': instance.deletedAt?.toIso8601String(),
      'id': instance.id,
      'updated_at': instance.updatedAt.toIso8601String(),
    };

Contact _$ContactFromJson(Map<String, dynamic> json) {
  return Contact(
    name: json['name'] as String,
    createdAt: DateTime.parse(json['created_at'] as String),
    updatedAt: DateTime.parse(json['updated_at'] as String),
    credit: (json['credit'] as num?)?.toDouble(),
    daysCreditTerm: json['days_credit_term'] as int?,
    adress: json['adress'] as String?,
    email: json['email'] as String?,
    taxIdentification: json['tax_identification'] as String?,
    deletedAt: json['deleted_at'] == null
        ? null
        : DateTime.parse(json['deleted_at'] as String),
    id: json['id'] as int?,
  );
}

Map<String, dynamic> _$ContactToJson(Contact instance) => <String, dynamic>{
      'name': instance.name,
      'credit': instance.credit,
      'days_credit_term': instance.daysCreditTerm,
      'adress': instance.adress,
      'email': instance.email,
      'tax_identification': instance.taxIdentification,
      'created_at': instance.createdAt.toIso8601String(),
      'deleted_at': instance.deletedAt?.toIso8601String(),
      'id': instance.id,
      'updated_at': instance.updatedAt.toIso8601String(),
    };

ProductPurchase _$ProductPurchaseFromJson(Map<String, dynamic> json) {
  return ProductPurchase(
    purchaseOrderId: json['purchase_order_id'] as int,
    productId: json['product_id'] as int,
    quantity: json['quantity'] as int,
    price: (json['price'] as num).toDouble(),
  );
}

Map<String, dynamic> _$ProductPurchaseToJson(ProductPurchase instance) =>
    <String, dynamic>{
      'purchase_order_id': instance.purchaseOrderId,
      'product_id': instance.productId,
      'quantity': instance.quantity,
      'price': instance.price,
    };

ProductSale _$ProductSaleFromJson(Map<String, dynamic> json) {
  return ProductSale(
    saleVoucherId: json['sale_voucher_id'] as int,
    productId: json['product_id'] as int,
    price: (json['price'] as num).toDouble(),
    quantity: (json['quantity'] as num).toDouble(),
    createdAt: DateTime.parse(json['created_at'] as String),
    updatedAt: DateTime.parse(json['updated_at'] as String),
  );
}

Map<String, dynamic> _$ProductSaleToJson(ProductSale instance) =>
    <String, dynamic>{
      'sale_voucher_id': instance.saleVoucherId,
      'product_id': instance.productId,
      'price': instance.price,
      'quantity': instance.quantity,
      'created_at': instance.createdAt.toIso8601String(),
      'updated_at': instance.updatedAt.toIso8601String(),
    };

Stock _$StockFromJson(Map<String, dynamic> json) {
  return Stock(
    productId: json['product_id'] as int,
    branchOfficeId: json['branch_office_id'] as int,
    quantity: json['quantity'] as int,
    lowWarning: json['low_warning'] as int,
    createdAt: DateTime.parse(json['created_at'] as String),
    updatedAt: DateTime.parse(json['updated_at'] as String),
  );
}

Map<String, dynamic> _$StockToJson(Stock instance) => <String, dynamic>{
      'product_id': instance.productId,
      'branch_office_id': instance.branchOfficeId,
      'quantity': instance.quantity,
      'low_warning': instance.lowWarning,
      'created_at': instance.createdAt.toIso8601String(),
      'updated_at': instance.updatedAt.toIso8601String(),
    };
