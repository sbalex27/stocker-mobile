library models;

import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

// Models
part 'src/auth.dart';
part 'src/product.dart';
part 'src/brand.dart';
part 'src/category.dart';
part 'src/user.dart';
part 'src/purchase_order.dart';
part 'src/sale_voucher.dart';
part 'src/branch_office.dart';
part 'src/contact.dart';

// Pivots
part 'src/pivots/product_purchase.dart';
part 'src/pivots/product_sale.dart';
part 'src/pivots/stock.dart';

// Interfaces
part 'src/interfaces/identifiable.dart';
part 'src/interfaces/time_stampable.dart';
part 'src/interfaces/soft_deletable.dart';
part 'src/interfaces/model.dart';
part 'src/interfaces/serializable.dart';
part 'src/interfaces/copyable.dart';

// Serialization
part 'models.g.dart';
