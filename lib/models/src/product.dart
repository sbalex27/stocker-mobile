part of models;

@JsonSerializable()
class Product extends Model with EquatableMixin, ChangeNotifier {
  int brandId;
  int categoryId;
  String name;
  double value;
  double gain;
  bool discountable;
  Brand? brand;
  Category? category;
  List<BranchOffice>? branchOffices;
  List<SaleVoucher>? sales;
  List<PurchaseOrder>? purchases;

  @override
  DateTime createdAt;

  @override
  DateTime? deletedAt;

  @override
  int? id;

  @override
  DateTime updatedAt;

  Product({
    required this.id,
    required this.brandId,
    required this.categoryId,
    required this.name,
    required this.value,
    required this.gain,
    required this.discountable,
    required this.createdAt,
    required this.updatedAt,
    this.sales,
    this.purchases,
    this.branchOffices,
    this.deletedAt,
    this.brand,
    this.category,
  });

  factory Product.fromJson(Map<String, dynamic> json) =>
      _$ProductFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$ProductToJson(this);

  @override
  List<Object?> get props => [
        brandId,
        categoryId,
        name,
        value,
        gain,
        discountable,
        brand,
        category,
        branchOffices,
        sales,
        purchases,
        ...commonProps
      ];

  @override
  void copy(Object other) {
    if (other is Product && other != this) {
      super.copy(other);
      brandId = other.brandId;
      categoryId = other.categoryId;
      name = other.name;
      value = other.value;
      gain = other.gain;
      discountable = other.discountable;
      brand = other.brand;
      category = other.category;
      branchOffices = other.branchOffices;
      sales = other.sales;
      purchases = other.purchases;
      notifyListeners();
    }
  }

  @override
  String toString() {
    return "$id - $name";
  }
}
