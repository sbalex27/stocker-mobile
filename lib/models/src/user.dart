part of models;

enum UserRole { admin, cashier }

@JsonSerializable()
class User
    with EquatableMixin
    implements Identifiable, Serializable, TimeStampable, SoftDeletable {
  String firstname;
  String? lastname;
  String email;
  UserRole role;

  @override
  DateTime createdAt;

  @override
  DateTime? deletedAt;

  @override
  int? id;

  @override
  DateTime updatedAt;

  User({
    required this.id,
    required this.firstname,
    required this.email,
    required this.role,
    required this.createdAt,
    required this.updatedAt,
    this.deletedAt,
    this.lastname,
  });

  factory User.fromJson(Map<String, dynamic> json) {
    return _$UserFromJson(json);
  }

  @override
  Map<String, dynamic> toJson() {
    return _$UserToJson(this);
  }

  @override
  List<Object?> get props =>
      [id, firstname, email, role, createdAt, updatedAt, deletedAt, lastname];

  @override
  String toString() {
    return '$firstname $lastname';
  }
}
