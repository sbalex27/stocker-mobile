part of models;

@JsonSerializable()
class Brand extends Model with EquatableMixin {
  String name;
  String? description;

  @override
  DateTime createdAt;

  @override
  DateTime? deletedAt;

  @override
  int? id;

  @override
  DateTime updatedAt;

  Brand({
    required this.id,
    required this.name,
    required this.createdAt,
    required this.updatedAt,
    this.deletedAt,
    this.description,
  });

  factory Brand.fromJson(Map<String, dynamic> json) {
    return _$BrandFromJson(json);
  }

  @override
  Map<String, dynamic> toJson() {
    return _$BrandToJson(this);
  }

  @override
  List<Object?> get props => [name, description, ...commonProps];

  @override
  void copy(Object other) {
    if (other is Brand && other != this) {
      super.copy(other);
      name = other.name;
      description = other.description;
    }
  }
}
