part of models;

@JsonSerializable()
class Contact extends Model {
  String name;
  double? credit;
  int? daysCreditTerm;
  String? adress;
  String? email;
  String? taxIdentification;

  @override
  DateTime createdAt;

  @override
  DateTime? deletedAt;

  @override
  int? id;

  @override
  DateTime updatedAt;

  @override
  Map<String, dynamic> toJson() {
    return _$ContactToJson(this);
  }

  Contact({
    required this.name,
    required this.createdAt,
    required this.updatedAt,
    this.credit,
    this.daysCreditTerm,
    this.adress,
    this.email,
    this.taxIdentification,
    this.deletedAt,
    this.id,
  });

  factory Contact.fromJson(Map<String, dynamic> json) {
    return _$ContactFromJson(json);
  }
}
