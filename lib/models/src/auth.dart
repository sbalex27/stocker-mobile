part of models;

@JsonSerializable(createFactory: false)
class Auth implements Serializable {
  String? email;
  String? password;

  Auth({this.email, this.password});

  @override
  Map<String, dynamic> toJson() {
    return _$AuthToJson(this);
  }
}
