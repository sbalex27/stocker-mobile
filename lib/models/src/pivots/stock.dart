part of models;

@JsonSerializable()
class Stock implements TimeStampable, Serializable {
  int productId;
  int branchOfficeId;
  int quantity;
  int lowWarning;

  @override
  DateTime createdAt;

  @override
  DateTime updatedAt;

  @override
  Map<String, dynamic> toJson() {
    return _$StockToJson(this);
  }

  Stock({
    required this.productId,
    required this.branchOfficeId,
    required this.quantity,
    required this.lowWarning,
    required this.createdAt,
    required this.updatedAt,
  });

  factory Stock.fromJson(Map<String, dynamic> json) {
    return _$StockFromJson(json);
  }
}
