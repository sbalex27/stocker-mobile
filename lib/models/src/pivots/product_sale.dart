part of models;

@JsonSerializable()
class ProductSale implements TimeStampable, Serializable {
  int saleVoucherId;
  int productId;
  double price;
  double quantity;

  @override
  DateTime createdAt;

  @override
  DateTime updatedAt;

  ProductSale({
    required this.saleVoucherId,
    required this.productId,
    required this.price,
    required this.quantity,
    required this.createdAt,
    required this.updatedAt,
  });

  @override
  Map<String, dynamic> toJson() {
    return _$ProductSaleToJson(this);
  }

  factory ProductSale.fromJson(Map<String, dynamic> json) {
    return _$ProductSaleFromJson(json);
  }
}
