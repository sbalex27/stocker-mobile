part of models;

@JsonSerializable()
class ProductPurchase extends Serializable {
  int purchaseOrderId;
  int productId;
  int quantity;
  double price;

  @override
  Map<String, dynamic> toJson() {
    return _$ProductPurchaseToJson(this);
  }

  ProductPurchase({
    required this.purchaseOrderId,
    required this.productId,
    required this.quantity,
    required this.price,
  });

  factory ProductPurchase.fromJson(Map<String, dynamic> json) {
    return _$ProductPurchaseFromJson(json);
  }
}
