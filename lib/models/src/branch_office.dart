part of models;

@JsonSerializable()
class BranchOffice extends Model {
  String name;
  String adress;
  String phone;
  Stock? stock;

  @override
  DateTime createdAt;

  @override
  DateTime? deletedAt;

  @override
  int? id;

  @override
  DateTime updatedAt;

  @override
  Map<String, dynamic> toJson() {
    return _$BranchOfficeToJson(this);
  }

  BranchOffice({
    required this.name,
    required this.adress,
    required this.phone,
    required this.createdAt,
    required this.updatedAt,
    this.stock,
    this.deletedAt,
    this.id,
  });
  
  factory BranchOffice.fromJson(Map<String, dynamic> json) {
    return _$BranchOfficeFromJson(json);
  }
}
