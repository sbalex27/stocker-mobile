part of models;

@JsonSerializable()
class Category extends Model with EquatableMixin {
  String name;
  String? description;

  @override
  DateTime createdAt;

  @override
  DateTime? deletedAt;

  @override
  int? id;

  @override
  DateTime updatedAt;

  Category({
    required this.id,
    required this.name,
    required this.createdAt,
    required this.updatedAt,
    this.deletedAt,
    this.description,
  });

  factory Category.fromJson(Map<String, dynamic> json) {
    return _$CategoryFromJson(json);
  }

  @override
  Map<String, dynamic> toJson() {
    return _$CategoryToJson(this);
  }

  @override
  List<Object?> get props => [name, description, ...commonProps];

  @override
  void copy(Object other) {
    if (other is Category && other != this) {
      super.copy(other);
      name = other.name;
      description = other.description;
    }
  }
}
