part of models;

abstract class Copyable {
  /// Copy the object properties, usually implements
  /// ```
  /// notifyListeners()
  /// ```
  void copy(Object other);
}
