part of models;

abstract class SoftDeletable {
  /// The deleted at resource timestamp
  DateTime? deletedAt;
}
