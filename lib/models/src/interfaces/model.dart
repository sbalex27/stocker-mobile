part of models;

abstract class Model
    implements
        Identifiable,
        TimeStampable,
        SoftDeletable,
        Copyable,
        Serializable {
  @override
  void copy(Object other) {
    if (other is Model) {
      id = other.id;
      createdAt = other.createdAt;
      updatedAt = other.updatedAt;
      deletedAt = other.deletedAt;
    }
  }

  List<Object?> get commonProps => [id, createdAt, updatedAt, deletedAt];
}
