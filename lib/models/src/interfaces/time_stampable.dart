part of models;

abstract class TimeStampable {
  /// The created timestamp resource
  late DateTime createdAt;

  /// The updated timestamp resource
  late DateTime updatedAt;
}
