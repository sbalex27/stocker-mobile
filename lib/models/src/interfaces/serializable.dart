part of models;

abstract class Serializable {
  /// Make an map with the values serializated
  Map<String, dynamic> toJson();
}
