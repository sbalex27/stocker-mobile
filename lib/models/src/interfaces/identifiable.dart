part of models;

abstract class Identifiable {
  /// The primary key database register
  late int? id;
}
