part of models;

enum PurchaseOrderStatus {
  undelivered,
  delivered,
}

@JsonSerializable()
class PurchaseOrder extends Model {
  int contactId;
  int branchOfficeId;
  int byUserId;
  User? byUser;
  PurchaseOrderStatus status;
  DateTime? dueDate;
  ProductPurchase? productPurchase;
  Contact? contact;
  BranchOffice? branchOffice;

  @override
  DateTime createdAt;

  @override
  DateTime? deletedAt;

  @override
  int? id;

  @override
  DateTime updatedAt;

  @override
  Map<String, dynamic> toJson() {
    return _$PurchaseOrderToJson(this);
  }

  PurchaseOrder({
    required this.contactId,
    required this.branchOfficeId,
    required this.byUserId,
    required this.status,
    required this.createdAt,
    required this.updatedAt,
    this.branchOffice,
    this.contact,
    this.productPurchase,
    this.byUser,
    this.dueDate,
    this.deletedAt,
    this.id,
  });

  factory PurchaseOrder.fromJson(Map<String, dynamic> json) {
    return _$PurchaseOrderFromJson(json);
  }
}
