part of models;

enum SaleVoucherPaymentType { cash, credit }

@JsonSerializable()
class SaleVoucher extends Model {
  int contactId;
  int? discountId;
  SaleVoucherPaymentType paymentType;
  DateTime? dueDate;
  ProductSale? productSale;
  Contact? contact;
  int branchOfficeId;
  BranchOffice? branchOffice;

  @override
  DateTime createdAt;

  @override
  DateTime? deletedAt;

  @override
  int? id;

  @override
  DateTime updatedAt;

  @override
  Map<String, dynamic> toJson() {
    return _$SaleVoucherToJson(this);
  }

  SaleVoucher({
    required this.contactId,
    required this.paymentType,
    required this.createdAt,
    required this.updatedAt,
    required this.branchOfficeId,
    this.branchOffice,
    this.productSale,
    this.discountId,
    this.dueDate,
    this.deletedAt,
    this.id,
  });

  factory SaleVoucher.fromJson(Map<String, dynamic> json) {
    return _$SaleVoucherFromJson(json);
  }
}
