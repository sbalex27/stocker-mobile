import 'package:faker/faker.dart';
import 'package:stocker/models/models.dart';
import 'package:stocker/services/api/api.dart';

extension Models on Faker {
  Product product(int? id) {
    return Product(
      id: id,
      brandId: randomGenerator.integer(100),
      categoryId: randomGenerator.integer(100),
      name: lorem.sentence(),
      value:
          double.parse(randomGenerator.decimal(scale: 10).toStringAsFixed(2)),
      gain: double.parse(randomGenerator.decimal().toStringAsFixed(1)),
      discountable: randomGenerator.boolean(),
      createdAt: date.dateTime(),
      updatedAt: date.dateTime(),
    );
  }

  Brand brand(int? id) {
    return Brand(
      id: id,
      name: lorem.word(),
      description: lorem.sentence(),
      createdAt: date.dateTime(),
      updatedAt: date.dateTime(),
    );
  }

  Category category(int? id) {
    return Category(
      id: id,
      name: lorem.word().toUpperCase(),
      description: lorem.sentence(),
      createdAt: date.dateTime(),
      updatedAt: date.dateTime(),
    );
  }

  User user(int? id) {
    return User(
      id: id,
      firstname: person.name(),
      email: internet.email(),
      role: UserRole.admin,
      createdAt: date.dateTime(),
      updatedAt: date.dateTime(),
    );
  }
}

extension Responses on Faker {
  PaginatedResource<T> paginatedResource<T>({
    required List<T> data,
    int perPage = 10,
    int page = 1,
  }) {
    final from = perPage * (page - 1);
    final to = perPage * page;
    final total = data.length;

    return PaginatedResource(
      currentPage: page,
      data: data.getRange(from, to).toList(),
      firstPageUrl: Uri.parse(internet.httpUrl()),
      from: from + 1,
      lastPage: (total / perPage).ceil(),
      lastPageUrl: Uri.parse(internet.httpUrl()),
      prevPageUrl: Uri.parse(internet.httpUrl()),
      path: Uri.parse(internet.httpUrl()),
      perPage: perPage,
      to: to,
      total: total,
    );
  }
}
