import 'package:stocker/lang/l10n.dart';
import 'package:stocker/models/models.dart';

extension Enumerable on Language {
  String userRole(UserRole role) {
    return {
      UserRole.admin: admin,
      UserRole.cashier: cashier,
    }[role]!;
  }
}
