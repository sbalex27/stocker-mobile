import 'package:flutter/material.dart';

extension Textable on String {
  /// Equivalent of:
  /// ```
  /// String value = 'Hello World';
  /// return Text(value);
  /// ```
  Text toWidget() => Text(this);
}

extension Iconable on IconData {
  /// Equivalent of:
  /// ```
  /// IconData value = Icons.person;
  /// return Icon(value);
  /// ```
  Icon toWidget() => Icon(this);
}
