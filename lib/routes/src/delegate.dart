part of routes;

BeamerDelegate buildDelegate([String initialRoute = '/']) {
  return BeamerDelegate(
    locationBuilder: BeamerLocationBuilder(
      beamLocations: [
        HomeLocation(),
        LoginLocation(),
        NotFoundLocation(),
        ProductLocation(),
        ProfileLocation(),
      ],
    ),
    initialPath: initialRoute,
    guards: [
      BeamGuard(
        pathBlueprints: ['/login'],
        guardNonMatching: true,
        check: (context, state) {
          return context.read<Authentication>().isAuthenticated;
        },
        beamToNamed: '/login',
      ),
    ],
    notFoundRedirect: NotFoundLocation(),
  );
}
