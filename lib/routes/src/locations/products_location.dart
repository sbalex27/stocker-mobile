part of routes;

class ProductLocation extends BeamLocation {
  static String productId = 'productId';
  static String initialData = 'initialData';
  static String timeline = 'timeline';

  @override
  List<BeamPage> buildPages(BuildContext context, BeamState state) {
    // State data
    final productId = state.pathParameters[ProductLocation.productId];
    final initialData = state.data[ProductLocation.initialData];

    return [
      ...HomeLocation().buildPages(context, state),
      BeamPage(
        name: 'Products',
        key: const ValueKey('products'),
        child: const ProductsScreen(),
      ),
      if (state.pathParameters.containsKey(ProductLocation.productId))
        BeamPage(
          key: ValueKey('product-$productId'),
          child: ProductDetailsScreen(
            id: int.parse(productId!),
            initialData: initialData as Product?,
          ),
        ),
      if (state.uri.pathSegments.contains(timeline))
        BeamPage(
          key: ValueKey('product-$productId-timeline'),
          child: const ProductTimelineScreen(),
        ),
    ];
  }

  @override
  List get pathBlueprints => ['/products/:productId/timeline'];
}
