part of routes;

class LoginLocation extends BeamLocation {
  @override
  List<BeamPage> buildPages(BuildContext context, BeamState state) {
    return [
      BeamPage(
        title: Language.of(context).login,
        key: const ValueKey(login),
        child: const LoginScreen(),
      ),
    ];
  }

  @override
  List get pathBlueprints => ['/login'];

  @override
  List<BeamGuard> get guards => [
        ...super.guards,
        BeamGuard(
          pathBlueprints: ['/login'],
          check: (context, state) {
            return !context.read<Authentication>().isAuthenticated;
          },
          beamToNamed: '/',
        )
      ];
}
