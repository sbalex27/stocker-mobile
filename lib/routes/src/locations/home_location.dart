part of routes;

List<String?> homeTabs = [null, products, purchases, sales];
const homeTabQuery = 'tab';

class HomeLocation extends BeamLocation {
  @override
  List<BeamPage> buildPages(BuildContext context, BeamState state) {
    final initialIndex = state.queryParameters.containsKey(homeTabQuery)
        ? homeTabs.indexOf(state.queryParameters[homeTabQuery])
        : 0;

    return [
      BeamPage(
        key: const ValueKey('home'),
        title: kAppName,
        child: HomeScreen(
          initialIndex: initialIndex,
        ),
      ),
    ];
  }

  @override
  List get pathBlueprints => ['/'];
}
