part of routes;

class NotFoundLocation extends BeamLocation {
  @override
  List<BeamPage> buildPages(BuildContext context, BeamState state) {
    return [
      BeamPage(
        key: ValueKey(state.uri),
        title: 'Not Found',
        child: const UnknownScreen(),
      ),
    ];
  }

  @override
  List get pathBlueprints => ['/404'];
}
