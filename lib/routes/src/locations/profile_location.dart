part of routes;

class ProfileLocation extends BeamLocation {
  @override
  List<BeamPage> buildPages(BuildContext context, BeamState state) {
    return [
      ...HomeLocation().buildPages(context, state),
      if (state.uri.pathSegments.contains(profile))
        BeamPage(
          key: const ValueKey(profile),
          child: const ProfileScreen(),
        ),
    ];
  }

  @override
  List get pathBlueprints => ['/profile'];
}
