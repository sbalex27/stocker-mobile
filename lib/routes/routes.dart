library routes;

import 'package:beamer/beamer.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
// ignore: implementation_imports
import 'package:provider/src/provider.dart';
import 'package:stocker/configurations/constrants.dart';
import 'package:stocker/lang/l10n.dart';
import 'package:stocker/models/models.dart';
import 'package:stocker/views/home/home_screen.dart';
import 'package:stocker/views/login/login_screen.dart';
import 'package:stocker/views/product_details/product_details_screen.dart';
import 'package:stocker/views/product_timeline/product_timeline_screen.dart';
import 'package:stocker/views/products/products_screen.dart';
import 'package:stocker/views/profile/profile_screen.dart';
import 'package:stocker/views/unknown/unknown_screen.dart';
import 'package:stocker/widgets/providers/authentication.dart';

part 'names.dart';
part 'src/guards/auth_guard.dart';
part 'src/locations/home_location.dart';
part 'src/locations/login_location.dart';
part 'src/locations/products_location.dart';
part 'src/locations/profile_location.dart';
part 'src/locations/not_found_location.dart';
part 'src/delegate.dart';
