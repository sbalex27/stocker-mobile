// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a es locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'es';

  static String m0(uri) =>
      "La página ${uri} que está buscando no existe, intente navegar por una dirección válida.";

  static String m1(field) => "El campo ${field} es requerido";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "admin": MessageLookupByLibrary.simpleMessage("Administrador"),
        "branchOffices": MessageLookupByLibrary.simpleMessage("Sucursales"),
        "brands": MessageLookupByLibrary.simpleMessage("Marcas"),
        "buyButton": MessageLookupByLibrary.simpleMessage("Comprar"),
        "carryOn": MessageLookupByLibrary.simpleMessage("Continuar"),
        "cashier": MessageLookupByLibrary.simpleMessage("Cajero"),
        "cashierBoxes": MessageLookupByLibrary.simpleMessage("Cajas"),
        "categories": MessageLookupByLibrary.simpleMessage("Categorías"),
        "charge": MessageLookupByLibrary.simpleMessage("Cobrar"),
        "contacts": MessageLookupByLibrary.simpleMessage("Contactos"),
        "dashboard": MessageLookupByLibrary.simpleMessage("Tablero"),
        "discounts": MessageLookupByLibrary.simpleMessage("Descuentos"),
        "email": MessageLookupByLibrary.simpleMessage("Correo Electrónico"),
        "errorCaptionRetry": MessageLookupByLibrary.simpleMessage(
            "Ha ocurrido un error, inténtelo de nuevo"),
        "errorTitle": MessageLookupByLibrary.simpleMessage("Ups!"),
        "firstname": MessageLookupByLibrary.simpleMessage("Nombres"),
        "lastname": MessageLookupByLibrary.simpleMessage("Apellidos"),
        "login": MessageLookupByLibrary.simpleMessage("Iniciar Sesión"),
        "logout": MessageLookupByLibrary.simpleMessage("Cerrar Sesión"),
        "logoutWarning": MessageLookupByLibrary.simpleMessage(
            "Esta acción cerrará su sesión actual en este dispositivo, si necesita volver a ingresar a la aplicación, se le pedirán sus credenciales."),
        "logs": MessageLookupByLibrary.simpleMessage("Registros"),
        "modelsProduct": MessageLookupByLibrary.simpleMessage("Producto"),
        "more": MessageLookupByLibrary.simpleMessage("Más"),
        "ok": MessageLookupByLibrary.simpleMessage("Aceptar"),
        "pageNotFoundCaption": m0,
        "pageNotFoundTitle":
            MessageLookupByLibrary.simpleMessage("Página no Encontrada"),
        "password": MessageLookupByLibrary.simpleMessage("Contraseña"),
        "pay": MessageLookupByLibrary.simpleMessage("Pagar"),
        "payments": MessageLookupByLibrary.simpleMessage("Pagos"),
        "phones": MessageLookupByLibrary.simpleMessage("Teléfonos"),
        "productDetails":
            MessageLookupByLibrary.simpleMessage("Detalles del Producto"),
        "products": MessageLookupByLibrary.simpleMessage("Productos"),
        "profile": MessageLookupByLibrary.simpleMessage("Perfil"),
        "purchase": MessageLookupByLibrary.simpleMessage("Comprar"),
        "purchaseOrders":
            MessageLookupByLibrary.simpleMessage("Ordenes de Compras"),
        "purchaseOrdersShort": MessageLookupByLibrary.simpleMessage("Compras"),
        "purchases": MessageLookupByLibrary.simpleMessage("Compras"),
        "references": MessageLookupByLibrary.simpleMessage("Referencias"),
        "requiredField": m1,
        "returnToHome": MessageLookupByLibrary.simpleMessage("Volver a Inicio"),
        "role": MessageLookupByLibrary.simpleMessage("Rol"),
        "salePayments": MessageLookupByLibrary.simpleMessage("Cobros"),
        "saleVouchers": MessageLookupByLibrary.simpleMessage("Vales de Ventas"),
        "saleVouchersShort": MessageLookupByLibrary.simpleMessage("Ventas"),
        "sales": MessageLookupByLibrary.simpleMessage("Ventas"),
        "seeAllButton": MessageLookupByLibrary.simpleMessage("Ver Todos"),
        "seeAllTooltip":
            MessageLookupByLibrary.simpleMessage("Vea todo los registros"),
        "sell": MessageLookupByLibrary.simpleMessage("Vender"),
        "sellButton": MessageLookupByLibrary.simpleMessage("Vender"),
        "stock": MessageLookupByLibrary.simpleMessage("Existencias"),
        "stocks": MessageLookupByLibrary.simpleMessage("Stocks"),
        "timelineButton":
            MessageLookupByLibrary.simpleMessage("Línea de Tiempo"),
        "timelineTooltip": MessageLookupByLibrary.simpleMessage(
            "Historial de acciones realizadas"),
        "transfers": MessageLookupByLibrary.simpleMessage("Movimientos"),
        "users": MessageLookupByLibrary.simpleMessage("Usuarios")
      };
}
