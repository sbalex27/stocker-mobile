// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  static String m0(uri) =>
      "The page ${uri} you are looking for does not exist, please try to browse by a valid address.";

  static String m1(field) => "The ${field} field is required";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "admin": MessageLookupByLibrary.simpleMessage("Administrator"),
        "branchOffices": MessageLookupByLibrary.simpleMessage("Branch Offices"),
        "brands": MessageLookupByLibrary.simpleMessage("Brands"),
        "buyButton": MessageLookupByLibrary.simpleMessage("Buy"),
        "carryOn": MessageLookupByLibrary.simpleMessage("Continue"),
        "cashier": MessageLookupByLibrary.simpleMessage("Cashier"),
        "cashierBoxes": MessageLookupByLibrary.simpleMessage("Cashier Boxes"),
        "categories": MessageLookupByLibrary.simpleMessage("Categories"),
        "charge": MessageLookupByLibrary.simpleMessage("Charge"),
        "contacts": MessageLookupByLibrary.simpleMessage("Contacts"),
        "dashboard": MessageLookupByLibrary.simpleMessage("Dashboard"),
        "discounts": MessageLookupByLibrary.simpleMessage("Discounts"),
        "email": MessageLookupByLibrary.simpleMessage("Email"),
        "errorCaptionRetry": MessageLookupByLibrary.simpleMessage(
            "An error is ocurred, try again"),
        "errorTitle": MessageLookupByLibrary.simpleMessage("Wops!"),
        "firstname": MessageLookupByLibrary.simpleMessage("Firstname"),
        "lastname": MessageLookupByLibrary.simpleMessage("Lastname"),
        "login": MessageLookupByLibrary.simpleMessage("Login"),
        "logout": MessageLookupByLibrary.simpleMessage("Logout"),
        "logoutWarning": MessageLookupByLibrary.simpleMessage(
            "This action will close your current session on this device, if you need to re-enter the application you will be asked for your credentials."),
        "logs": MessageLookupByLibrary.simpleMessage("Logs"),
        "modelsProduct": MessageLookupByLibrary.simpleMessage("Product"),
        "more": MessageLookupByLibrary.simpleMessage("More"),
        "ok": MessageLookupByLibrary.simpleMessage("Ok"),
        "pageNotFoundCaption": m0,
        "pageNotFoundTitle":
            MessageLookupByLibrary.simpleMessage("Page Not Found"),
        "password": MessageLookupByLibrary.simpleMessage("Password"),
        "pay": MessageLookupByLibrary.simpleMessage("Pay"),
        "payments": MessageLookupByLibrary.simpleMessage("Payments"),
        "phones": MessageLookupByLibrary.simpleMessage("Phones"),
        "productDetails":
            MessageLookupByLibrary.simpleMessage("Product Details"),
        "products": MessageLookupByLibrary.simpleMessage("Products"),
        "profile": MessageLookupByLibrary.simpleMessage("Profile"),
        "purchase": MessageLookupByLibrary.simpleMessage("Purchase"),
        "purchaseOrders":
            MessageLookupByLibrary.simpleMessage("Purchase Orders"),
        "purchaseOrdersShort":
            MessageLookupByLibrary.simpleMessage("Purchases"),
        "purchases": MessageLookupByLibrary.simpleMessage("Purchases"),
        "references": MessageLookupByLibrary.simpleMessage("References"),
        "requiredField": m1,
        "returnToHome": MessageLookupByLibrary.simpleMessage("Back Home"),
        "role": MessageLookupByLibrary.simpleMessage("Role"),
        "salePayments": MessageLookupByLibrary.simpleMessage("Sale Payments"),
        "saleVouchers": MessageLookupByLibrary.simpleMessage("Sale Vouchers"),
        "saleVouchersShort": MessageLookupByLibrary.simpleMessage("Sales"),
        "sales": MessageLookupByLibrary.simpleMessage("Sales"),
        "seeAllButton": MessageLookupByLibrary.simpleMessage("See All"),
        "seeAllTooltip":
            MessageLookupByLibrary.simpleMessage("See all records"),
        "sell": MessageLookupByLibrary.simpleMessage("Sell"),
        "sellButton": MessageLookupByLibrary.simpleMessage("Sell"),
        "stock": MessageLookupByLibrary.simpleMessage("Stock"),
        "stocks": MessageLookupByLibrary.simpleMessage("Stocks"),
        "timelineButton": MessageLookupByLibrary.simpleMessage("Timeline"),
        "timelineTooltip": MessageLookupByLibrary.simpleMessage(
            "History of actions carried out"),
        "transfers": MessageLookupByLibrary.simpleMessage("Transfers"),
        "users": MessageLookupByLibrary.simpleMessage("Users")
      };
}
