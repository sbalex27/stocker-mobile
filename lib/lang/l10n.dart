// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class Language {
  Language();

  static Language? _current;

  static Language get current {
    assert(_current != null,
        'No instance of Language was loaded. Try to initialize the Language delegate before accessing Language.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<Language> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = Language();
      Language._current = instance;

      return instance;
    });
  }

  static Language of(BuildContext context) {
    final instance = Language.maybeOf(context);
    assert(instance != null,
        'No instance of Language present in the widget tree. Did you add Language.delegate in localizationsDelegates?');
    return instance!;
  }

  static Language? maybeOf(BuildContext context) {
    return Localizations.of<Language>(context, Language);
  }

  /// `Administrator`
  String get admin {
    return Intl.message(
      'Administrator',
      name: 'admin',
      desc: 'Used in user role',
      args: [],
    );
  }

  /// `Branch Offices`
  String get branchOffices {
    return Intl.message(
      'Branch Offices',
      name: 'branchOffices',
      desc: '',
      args: [],
    );
  }

  /// `Brands`
  String get brands {
    return Intl.message(
      'Brands',
      name: 'brands',
      desc: '',
      args: [],
    );
  }

  /// `Buy`
  String get buyButton {
    return Intl.message(
      'Buy',
      name: 'buyButton',
      desc: '',
      args: [],
    );
  }

  /// `Continue`
  String get carryOn {
    return Intl.message(
      'Continue',
      name: 'carryOn',
      desc: 'Continue',
      args: [],
    );
  }

  /// `Cashier`
  String get cashier {
    return Intl.message(
      'Cashier',
      name: 'cashier',
      desc: '',
      args: [],
    );
  }

  /// `Cashier Boxes`
  String get cashierBoxes {
    return Intl.message(
      'Cashier Boxes',
      name: 'cashierBoxes',
      desc: '',
      args: [],
    );
  }

  /// `Categories`
  String get categories {
    return Intl.message(
      'Categories',
      name: 'categories',
      desc: '',
      args: [],
    );
  }

  /// `Charge`
  String get charge {
    return Intl.message(
      'Charge',
      name: 'charge',
      desc: '',
      args: [],
    );
  }

  /// `Contacts`
  String get contacts {
    return Intl.message(
      'Contacts',
      name: 'contacts',
      desc: '',
      args: [],
    );
  }

  /// `Dashboard`
  String get dashboard {
    return Intl.message(
      'Dashboard',
      name: 'dashboard',
      desc: '',
      args: [],
    );
  }

  /// `Discounts`
  String get discounts {
    return Intl.message(
      'Discounts',
      name: 'discounts',
      desc: '',
      args: [],
    );
  }

  /// `Email`
  String get email {
    return Intl.message(
      'Email',
      name: 'email',
      desc: 'Email Field',
      args: [],
    );
  }

  /// `An error is ocurred, try again`
  String get errorCaptionRetry {
    return Intl.message(
      'An error is ocurred, try again',
      name: 'errorCaptionRetry',
      desc: 'Error with try again',
      args: [],
    );
  }

  /// `Wops!`
  String get errorTitle {
    return Intl.message(
      'Wops!',
      name: 'errorTitle',
      desc: 'Error title, normally used in alert dialog title',
      args: [],
    );
  }

  /// `Firstname`
  String get firstname {
    return Intl.message(
      'Firstname',
      name: 'firstname',
      desc: '',
      args: [],
    );
  }

  /// `Lastname`
  String get lastname {
    return Intl.message(
      'Lastname',
      name: 'lastname',
      desc: '',
      args: [],
    );
  }

  /// `Login`
  String get login {
    return Intl.message(
      'Login',
      name: 'login',
      desc: '',
      args: [],
    );
  }

  /// `Logout`
  String get logout {
    return Intl.message(
      'Logout',
      name: 'logout',
      desc: '',
      args: [],
    );
  }

  /// `This action will close your current session on this device, if you need to re-enter the application you will be asked for your credentials.`
  String get logoutWarning {
    return Intl.message(
      'This action will close your current session on this device, if you need to re-enter the application you will be asked for your credentials.',
      name: 'logoutWarning',
      desc: '',
      args: [],
    );
  }

  /// `Logs`
  String get logs {
    return Intl.message(
      'Logs',
      name: 'logs',
      desc: '',
      args: [],
    );
  }

  /// `Product`
  String get modelsProduct {
    return Intl.message(
      'Product',
      name: 'modelsProduct',
      desc: '',
      args: [],
    );
  }

  /// `More`
  String get more {
    return Intl.message(
      'More',
      name: 'more',
      desc: '',
      args: [],
    );
  }

  /// `Ok`
  String get ok {
    return Intl.message(
      'Ok',
      name: 'ok',
      desc: '',
      args: [],
    );
  }

  /// `The page {uri} you are looking for does not exist, please try to browse by a valid address.`
  String pageNotFoundCaption(Object uri) {
    return Intl.message(
      'The page $uri you are looking for does not exist, please try to browse by a valid address.',
      name: 'pageNotFoundCaption',
      desc: '',
      args: [uri],
    );
  }

  /// `Page Not Found`
  String get pageNotFoundTitle {
    return Intl.message(
      'Page Not Found',
      name: 'pageNotFoundTitle',
      desc: '',
      args: [],
    );
  }

  /// `Password`
  String get password {
    return Intl.message(
      'Password',
      name: 'password',
      desc: 'Password Field',
      args: [],
    );
  }

  /// `Pay`
  String get pay {
    return Intl.message(
      'Pay',
      name: 'pay',
      desc: '',
      args: [],
    );
  }

  /// `Payments`
  String get payments {
    return Intl.message(
      'Payments',
      name: 'payments',
      desc: '',
      args: [],
    );
  }

  /// `Phones`
  String get phones {
    return Intl.message(
      'Phones',
      name: 'phones',
      desc: '',
      args: [],
    );
  }

  /// `Product Details`
  String get productDetails {
    return Intl.message(
      'Product Details',
      name: 'productDetails',
      desc: '',
      args: [],
    );
  }

  /// `Products`
  String get products {
    return Intl.message(
      'Products',
      name: 'products',
      desc: '',
      args: [],
    );
  }

  /// `Profile`
  String get profile {
    return Intl.message(
      'Profile',
      name: 'profile',
      desc: '',
      args: [],
    );
  }

  /// `Purchase`
  String get purchase {
    return Intl.message(
      'Purchase',
      name: 'purchase',
      desc: '',
      args: [],
    );
  }

  /// `Purchase Orders`
  String get purchaseOrders {
    return Intl.message(
      'Purchase Orders',
      name: 'purchaseOrders',
      desc: '',
      args: [],
    );
  }

  /// `Purchases`
  String get purchaseOrdersShort {
    return Intl.message(
      'Purchases',
      name: 'purchaseOrdersShort',
      desc: '',
      args: [],
    );
  }

  /// `Purchases`
  String get purchases {
    return Intl.message(
      'Purchases',
      name: 'purchases',
      desc: '',
      args: [],
    );
  }

  /// `References`
  String get references {
    return Intl.message(
      'References',
      name: 'references',
      desc: '',
      args: [],
    );
  }

  /// `The {field} field is required`
  String requiredField(Object field) {
    return Intl.message(
      'The $field field is required',
      name: 'requiredField',
      desc: 'Normally used in the validation rule',
      args: [field],
    );
  }

  /// `Back Home`
  String get returnToHome {
    return Intl.message(
      'Back Home',
      name: 'returnToHome',
      desc: 'Used in not found page',
      args: [],
    );
  }

  /// `Role`
  String get role {
    return Intl.message(
      'Role',
      name: 'role',
      desc: '',
      args: [],
    );
  }

  /// `Sale Payments`
  String get salePayments {
    return Intl.message(
      'Sale Payments',
      name: 'salePayments',
      desc: '',
      args: [],
    );
  }

  /// `Sales`
  String get sales {
    return Intl.message(
      'Sales',
      name: 'sales',
      desc: '',
      args: [],
    );
  }

  /// `Sale Vouchers`
  String get saleVouchers {
    return Intl.message(
      'Sale Vouchers',
      name: 'saleVouchers',
      desc: '',
      args: [],
    );
  }

  /// `Sales`
  String get saleVouchersShort {
    return Intl.message(
      'Sales',
      name: 'saleVouchersShort',
      desc: '',
      args: [],
    );
  }

  /// `See All`
  String get seeAllButton {
    return Intl.message(
      'See All',
      name: 'seeAllButton',
      desc: '',
      args: [],
    );
  }

  /// `See all records`
  String get seeAllTooltip {
    return Intl.message(
      'See all records',
      name: 'seeAllTooltip',
      desc: '',
      args: [],
    );
  }

  /// `Sell`
  String get sell {
    return Intl.message(
      'Sell',
      name: 'sell',
      desc: '',
      args: [],
    );
  }

  /// `Sell`
  String get sellButton {
    return Intl.message(
      'Sell',
      name: 'sellButton',
      desc: '',
      args: [],
    );
  }

  /// `Stock`
  String get stock {
    return Intl.message(
      'Stock',
      name: 'stock',
      desc: '',
      args: [],
    );
  }

  /// `Stocks`
  String get stocks {
    return Intl.message(
      'Stocks',
      name: 'stocks',
      desc: '',
      args: [],
    );
  }

  /// `Timeline`
  String get timelineButton {
    return Intl.message(
      'Timeline',
      name: 'timelineButton',
      desc: '',
      args: [],
    );
  }

  /// `History of actions carried out`
  String get timelineTooltip {
    return Intl.message(
      'History of actions carried out',
      name: 'timelineTooltip',
      desc: '',
      args: [],
    );
  }

  /// `Transfers`
  String get transfers {
    return Intl.message(
      'Transfers',
      name: 'transfers',
      desc: '',
      args: [],
    );
  }

  /// `Users`
  String get users {
    return Intl.message(
      'Users',
      name: 'users',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<Language> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'es'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<Language> load(Locale locale) => Language.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
