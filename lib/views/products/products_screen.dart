import 'package:beamer/beamer.dart';
import 'package:flutter/material.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:provider/provider.dart';
import 'package:stocker/models/models.dart';
import 'package:stocker/repositories/repositories.dart';
import 'package:stocker/widgets/mixins/is_set_mixin.dart';

part 'product_tile.dart';

class ProductsScreen extends StatefulWidget {
  const ProductsScreen({Key? key}) : super(key: key);

  @override
  _ProductsScreenState createState() => _ProductsScreenState();
}

class _ProductsScreenState extends State<ProductsScreen> {
  final _pagingController = PagingController<int, Product>(firstPageKey: 1);

  late ProductRepository _repository;

  @override
  void initState() {
    super.initState();
    _pagingController.addPageRequestListener((page) {
      _fetchPage(page);
    });
  }

  @override
  void dispose() {
    _pagingController.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _repository = Provider.of<ProductRepository>(context);
  }

  Future<void> _fetchPage(int page) async {
    try {
      final response = await _repository.index(page);
      if (mounted) {
        if (response.currentPage == response.lastPage) {
          _pagingController.appendLastPage(response.data);
        } else {
          _pagingController.appendPage(response.data, response.currentPage + 1);
        }
      }
    } on UnsuccessfulException catch (error) {
      if (mounted) {
        _pagingController.error = error.message;
      }
    } catch (e) {
      if (mounted) {
        _pagingController.error = "Error";
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Beamer.of(context).popToNamed('/?tab=products');
          },
        ),
      ),
      body: RefreshIndicator(
        onRefresh: () => Future.sync(() => _pagingController.refresh()),
        child: PagedListView(
          key: const Key('products_list'),
          addAutomaticKeepAlives: true,
          pagingController: _pagingController,
          builderDelegate: PagedChildBuilderDelegate<Product>(
            firstPageErrorIndicatorBuilder: (context) {
              return Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 32),
                child: ErrorContainer(
                  title: 'Test',
                  description: _pagingController.error.toString(),
                  onRetry: _pagingController.refresh,
                ),
              );
            },
            itemBuilder: (context, product, index) {
              return ProductTile(product: product);
            },
          ),
        ),
      ),
    );
  }
}

class ErrorContainer extends StatelessWidget {
  const ErrorContainer({
    Key? key,
    required this.title,
    required this.description,
    required this.onRetry,
  }) : super(key: key);

  final String title;
  final String description;
  final VoidCallback onRetry;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(title, style: Theme.of(context).textTheme.headline6),
        const SizedBox(height: 16),
        Text(
          description,
          textAlign: TextAlign.center,
        ),
        const SizedBox(height: 32),
        SizedBox(
          height: 48,
          width: double.infinity,
          child: ElevatedButton.icon(
            onPressed: onRetry,
            icon: const Icon(Icons.refresh),
            label: const Text('Retry'),
          ),
        )
      ],
    );
  }
}
