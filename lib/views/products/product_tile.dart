part of 'products_screen.dart';

class ProductTile extends StatelessWidget with IsSetMixin {
  const ProductTile({Key? key, required this.product}) : super(key: key);

  final Product product;

  @override
  Widget build(BuildContext context) {
    return Material(
      child: ListTile(
        title: Text(
          product.name,
          overflow: TextOverflow.ellipsis,
        ),
        subtitle: isSetBuilder<Brand>(
          value: product.brand,
          builder: (brand) => Text(brand.name),
        ),
        trailing: isSetBuilder<Category>(
          value: product.category,
          builder: (category) => Chip(label: Text(category.name)),
        ),
        onTap: () {
          Beamer.of(context).beamToNamed(
            '/products/${product.id}',
            data: {'initialData': product},
          );
        },
      ),
    );
  }
}
