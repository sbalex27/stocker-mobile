import 'package:beamer/beamer.dart';
import 'package:flutter/material.dart';
import 'package:stocker/lang/l10n.dart';
import 'package:stocker/widgets/informative_container.dart';

class UnknownScreen extends StatelessWidget {
  const UnknownScreen({
    Key? key,
    this.page,
  }) : super(key: key);

  final String? page;

  static const String asset = 'assets/illustrations/page_not_found.svg';

  @override
  Widget build(BuildContext context) {
    final uri = Beamer.of(context).state.uri.toString();

    return Scaffold(
      body: InformativeContainer(
        asset: asset,
        title: Language.of(context).pageNotFoundTitle,
        caption: Language.of(context).pageNotFoundCaption(uri),
        buttons: [
          OutlinedButton.icon(
            onPressed: () {
              Beamer.of(context).popToNamed('/');
            },
            icon: const Icon(Icons.arrow_back),
            label: Text(Language.of(context).returnToHome.toUpperCase()),
          ),
        ],
      ),
    );
  }
}
