import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:stocker/extensions/lang.dart';
import 'package:stocker/lang/l10n.dart';
import 'package:stocker/models/models.dart';
import 'package:stocker/widgets/providers/authentication.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final lang = Language.of(context);
    return Scaffold(
      body: Selector<Authentication, User?>(
        selector: (context, env) => env.user,
        builder: (context, auth, children) {
          return CustomScrollView(
            slivers: [
              SliverAppBar(
                expandedHeight: 200,
                pinned: true,
                flexibleSpace: FlexibleSpaceBar(
                  title: Text(lang.profile),
                ),
              ),
              if (auth != null)
                SliverList(
                  delegate: SliverChildListDelegate(
                    [
                      ListTile(
                        title: Text(lang.firstname),
                        subtitle: Text(auth.firstname),
                      ),
                      if (auth.lastname != null)
                        ListTile(
                          title: Text(lang.lastname),
                          subtitle: Text(auth.lastname!),
                        ),
                      ListTile(
                        title: Text(lang.email),
                        subtitle: Text(auth.email),
                      ),
                      ListTile(
                        title: Text(lang.role),
                        subtitle: Text(lang.userRole(auth.role)),
                      ),
                    ],
                  ),
                )
              else
                const SliverFillRemaining(
                  child: Center(child: CircularProgressIndicator()),
                )
            ],
          );
        },
      ),
    );
  }
}
