import 'package:flutter/material.dart';
import 'package:stocker/lang/l10n.dart';
import 'package:stocker/widgets/providers/model_icon_data.dart';
import 'package:timelines/timelines.dart';

class ProductTimelineScreen extends StatelessWidget {
  const ProductTimelineScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final lang = Language.of(context);
    final icons = ModelIconData.of(context)!;

    return Scaffold(
      appBar: AppBar(
        title: Text(lang.timelineButton),
      ),
      body: Timeline(
        children: [
          _Tile(
            icon: Icon(icons.purchaseOrder),
            title: Text(lang.purchase),
            subtitle: const Text('You purchase 5000 units'),
            timestamp: const Text('1/1/21'),
          ),
          _Tile(
            icon: Icon(icons.saleVoucher),
            title: Text(lang.sell),
            subtitle: const Text('You sale 5 units'),
            timestamp: const Text('2/3/21'),
          ),
          _Tile(
            icon: Icon(icons.stock),
            title: Text(lang.stock),
            subtitle: const Text('The user has changed from 5 to 3 units'),
            timestamp: const Text('4/5/21'),
          ),
          _Tile(
            icon: Icon(icons.transfer),
            title: Text(lang.transfers),
            subtitle: const Text('Transfered from central to sourth 40 units'),
            timestamp: const Text('20/5/21'),
          ),
          const _Tile(
            icon: Icon(Icons.edit),
            title: Text('Edit'),
            subtitle: Text('Changed from "Lorem" to "Rand"'),
            timestamp: Text('30/10/21'),
            isLast: true,
          ),
        ],
      ),
    );
  }
}

class _Tile extends StatelessWidget {
  const _Tile({
    Key? key,
    required this.icon,
    required this.title,
    required this.subtitle,
    required this.timestamp,
    this.isLast = false,
  }) : super(key: key);

  final Widget icon;
  final Widget title;
  final Widget subtitle;
  final Widget timestamp;
  final bool isLast;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final connector = SolidLineConnector(
      color: theme.colorScheme.primary,
    );
    return TimelineTile(
      node: TimelineNode(
        position: 0.1,
        startConnector: connector,
        endConnector: isLast ? null : connector,
        indicator: ContainerIndicator(
          child: CircleAvatar(
            backgroundColor: theme.colorScheme.primary,
            child: IconTheme(
              data: IconThemeData(color: theme.colorScheme.onPrimary),
              child: icon,
            ),
          ),
        ),
      ),
      contents: Card(
        margin: const EdgeInsets.all(12),
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              DefaultTextStyle(
                style: theme.textTheme.headline6!,
                child: title,
              ),
              const SizedBox(height: 6),
              subtitle,
              const SizedBox(height: 3),
              Row(
                children: [
                  Icon(
                    Icons.history,
                    size: theme.textTheme.caption!.fontSize,
                  ),
                  const SizedBox(width: 3),
                  DefaultTextStyle(
                    style: theme.textTheme.caption!,
                    child: timestamp,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
