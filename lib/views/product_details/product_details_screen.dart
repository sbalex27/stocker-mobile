import 'package:beamer/beamer.dart';
import 'package:faker/faker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:stocker/extensions/natives.dart';
import 'package:stocker/lang/l10n.dart';
import 'package:stocker/models/models.dart';
import 'package:stocker/repositories/repositories.dart';
import 'package:stocker/widgets/providers/model_icon_data.dart';

part 'card.dart';
part 'purchases_container.dart';
part 'sales_container.dart';
part 'stocks_container.dart';

class ProductDetailsScreen extends StatefulWidget {
  const ProductDetailsScreen({
    Key? key,
    this.initialData,
    required this.id,
  }) : super(key: key);

  final int id;
  final Product? initialData;

  @override
  State<ProductDetailsScreen> createState() => _ProductDetailsScreenState();
}

class _ProductDetailsScreenState extends State<ProductDetailsScreen> {
  Product? _product;

  Product? get product => _product;

  set product(Product? value) {
    setState(() {
      _product = value;
    });
  }

  @override
  void initState() {
    _product = widget.initialData;
    super.initState();
  }

  @override
  void didChangeDependencies() {
    fetch(context.read<ProductRepository>());
    super.didChangeDependencies();
  }

  Future<void> fetch(ProductRepository repo) {
    return repo.show(widget.id).then((product) {
      this.product?.copy(product);
      this.product ??= product;
    });
  }

  @override
  Widget build(BuildContext context) {
    final lang = Language.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(lang.productDetails),
        actions: [
          IconButton(
            icon: const Icon(Icons.edit),
            onPressed: () {},
          )
        ],
      ),
      body: ChangeNotifierProvider<Product?>.value(
        value: product,
        child: Center(
          child: ConstrainedBox(
            constraints: const BoxConstraints(
              maxWidth: 800,
            ),
            child: Builder(
              builder: (context) {
                if (product == null) {
                  return const Center(
                    child: CircularProgressIndicator.adaptive(),
                  );
                }
                return ListView(
                  children: [
                    Container(
                      margin: const EdgeInsets.all(16),
                      child: const _Header(),
                    ),
                    const Divider(),
                    const _Stocks(),
                    const _Sales(),
                    const _Purchases(),
                  ],
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}

class _Header extends StatelessWidget {
  const _Header({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final iconData = ModelIconData.of(context)!;
    final lang = Language.of(context);

    return Consumer<Product?>(
      builder: (context, product, child) {
        return Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        product!.name,
                        style: theme.textTheme.headline4,
                        overflow: TextOverflow.clip,
                      ),
                      if (product.brand != null) ...[
                        const SizedBox(height: 8),
                        Text(product.brand!.name),
                      ]
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 12),
                  child: Text(
                    'Q. ${product.value}',
                    style: theme.textTheme.headline6,
                  ),
                ),
              ],
            ),
            const SizedBox(height: 24),
            // Actions
            Align(
              alignment: Alignment.centerLeft,
              child: Theme(
                data: ThemeData(
                  chipTheme: ChipThemeData.fromDefaults(
                    primaryColor: theme.colorScheme.primary,
                    secondaryColor: theme.colorScheme.secondary,
                    labelStyle: theme.textTheme.bodyText1!,
                  ),
                  iconTheme: IconThemeData(
                    color: theme.colorScheme.primary,
                  ),
                ),
                child: Wrap(
                  spacing: 10,
                  children: [
                    if (product.category != null)
                      ActionChip(
                        avatar: Icon(iconData.category),
                        label: Text(product.category!.name.capitalize()),
                        onPressed: () {},
                      ),
                    ActionChip(
                      label: Text(lang.sellButton),
                      avatar: Icon(iconData.saleVoucher),
                      onPressed: () {},
                    ),
                    ActionChip(
                      label: Text(lang.buyButton),
                      avatar: Icon(iconData.purchaseOrder),
                      onPressed: () {},
                    ),
                    ActionChip(
                      label: Text(lang.timelineButton),
                      tooltip: lang.timelineTooltip,
                      avatar: const Icon(Icons.timeline),
                      onPressed: () {
                        Beamer.of(context)
                            .beamToNamed('/products/${product.id}/timeline');
                      },
                    )
                  ],
                ),
              ),
            )
          ],
        );
      },
    );
  }
}
