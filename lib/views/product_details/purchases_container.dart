part of 'product_details_screen.dart';

class _Purchases extends StatelessWidget {
  const _Purchases({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final lang = Language.of(context);
    return Selector<Product?, List<PurchaseOrder>>(
      selector: (context, product) => product?.purchases ?? [],
      builder: (context, purchases, child) {
        return _ContainerCard(
          title: lang.purchases,
          leading: OutlinedButton(
            onPressed: () {},
            child: Text(lang.seeAllButton),
          ),
          child: Column(
            children: purchases.map<Widget>((purchase) {
              return ListTile(
                title: Text(purchase.contact!.name),
                subtitle: Text(purchase.branchOffice!.name),
                trailing: Text(purchase.productPurchase!.quantity.toString()),
              );
            }).toList(),
          ),
        );
      },
    );
  }
}
