part of 'product_details_screen.dart';

class _Sales extends StatelessWidget {
  const _Sales({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final lang = Language.of(context);
    return Selector<Product?, List<SaleVoucher>?>(
      selector: (context, product) => product?.sales,
      builder: (context, sales, child) {
        return _ContainerCard(
          title: lang.sales,
          leading: OutlinedButton(
            onPressed: () {},
            child: Text(lang.seeAllButton),
          ),
          child: sales != null
              ? sales.isEmpty
                  ? const Text('Empty')
                  : _buildTiles(sales)
              : _buildPlaceholder(),
        );
      },
    );
  }

  Widget _buildTiles(List<SaleVoucher> sales) {
    return Column(
      children: sales.map<Widget>((sale) {
        return ListTile(
          title: Text(sale.contact!.name),
          subtitle: Text(sale.branchOffice!.name),
          trailing: Text(sale.productSale!.quantity.toString()),
        );
      }).toList(),
    );
  }

  Widget _buildPlaceholder() {
    return const Padding(
      padding: EdgeInsets.all(8.0),
      child: Center(child: CircularProgressIndicator.adaptive()),
    );
  }
}
