part of 'product_details_screen.dart';

class _Stocks extends StatelessWidget {
  const _Stocks({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final lang = Language.of(context);
    return PaginatedDataTable(
      header: Text(lang.stock),
      actions: [
        IconButton(
          onPressed: () {},
          icon: const Icon(Icons.delete),
        ),
      ],
      columns: const [
        DataColumn(
          label: Text('Office Branch'),
        ),
        DataColumn(
          label: Text('Low Warning'),
        ),
        DataColumn(
          label: Text('Quantity'),
        ),
      ],
      rowsPerPage: 4,
      source: _StocksDataSource.faker(),
    );
  }
}

class _StockRow {
  _StockRow({
    required this.officeBranch,
    required this.lowWarning,
    required this.quantity,
  });

  final String officeBranch;
  final int lowWarning;
  final int quantity;
  bool selected = false;
}

class _StocksDataSource extends DataTableSource {
  _StocksDataSource({
    required this.stockRows,
  });

  factory _StocksDataSource.faker() {
    final faker = Faker();
    return _StocksDataSource(
      stockRows: [
        _StockRow(
          officeBranch: faker.company.name(),
          lowWarning: 5,
          quantity: 2,
        ),
        _StockRow(
          officeBranch: faker.company.name(),
          lowWarning: 3,
          quantity: 40,
        ),
        _StockRow(
          officeBranch: faker.company.name(),
          lowWarning: 6,
          quantity: 30,
        ),
        _StockRow(
          officeBranch: faker.company.name(),
          lowWarning: 8,
          quantity: 110,
        ),
      ],
    );
  }

  final List<_StockRow> stockRows;
  int _selectedCount = 0;

  @override
  DataRow? getRow(int index) {
    assert(index >= 0);
    if (index <= stockRows.length) {
      final currentRow = stockRows[index];
      return DataRow.byIndex(
        index: index,
        selected: currentRow.selected,
        onSelectChanged: (value) {
          if (currentRow.selected != value) {
            _selectedCount += value ?? false ? 1 : -1;
            assert(_selectedCount >= 0);
            currentRow.selected = value ?? false;
            notifyListeners();
          }
        },
        cells: [
          DataCell(Text(currentRow.officeBranch)),
          DataCell(Text(currentRow.lowWarning.toString())),
          DataCell(Text(currentRow.quantity.toString())),
        ],
      );
    }
  }

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => stockRows.length;

  @override
  int get selectedRowCount => _selectedCount;
}
