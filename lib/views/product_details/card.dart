part of 'product_details_screen.dart';

class _ContainerCard extends StatelessWidget {
  const _ContainerCard({
    Key? key,
    required this.title,
    required this.child,
    this.leading,
  }) : super(key: key);

  final String title;
  final Widget? leading;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(
              left: 16,
              top: 8,
              right: 16,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  title,
                  style: Theme.of(context)
                      .textTheme
                      .headline6!
                      .copyWith(fontWeight: FontWeight.w400),
                ),
                if (leading != null) leading!,
              ],
            ),
          ),
          child,
        ],
      ),
    );
  }
}
