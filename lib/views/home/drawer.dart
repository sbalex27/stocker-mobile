part of 'home_screen.dart';

class _MainDrawer extends StatelessWidget {
  const _MainDrawer({
    Key? key,
    required this.extended,
    this.selected,
    this.onChanged,
  }) : super(key: key);

  final int? selected;
  final ValueChanged<int>? onChanged;
  final bool extended;

  @override
  Widget build(BuildContext context) {
    return Selector<Authentication, User?>(
      selector: (context, env) => env.user,
      builder: (context, auth, child) {
        final icons = ModelIconData.of(context)!;
        final lang = Language.of(context);
        final textTheme = Theme.of(context).textTheme;
        return Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: [
              UserAccountsDrawerHeader(
                decoration: extended ? const BoxDecoration() : null,
                accountName: _headerText(auth?.toString(), textTheme.subtitle1),
                accountEmail: _headerText(auth?.email, textTheme.subtitle2),
                currentAccountPicture: _accountCharacter(auth?.toString()),
              ),
              if (extended) ...[
                ListTile(
                  leading: const Icon(Icons.dashboard),
                  title: lang.dashboard.toWidget(),
                  selected: selected == 0,
                  onTap: () => onChanged?.call(0),
                ),
                ListTile(
                  leading: icons.product.toWidget(),
                  title: lang.products.toWidget(),
                  selected: selected == 1,
                  onTap: () => onChanged?.call(1),
                ),
                ListTile(
                  leading: icons.purchaseOrder.toWidget(),
                  title: lang.purchaseOrdersShort.toWidget(),
                  selected: selected == 2,
                  onTap: () => onChanged?.call(2),
                ),
                ListTile(
                  leading: icons.saleVoucher.toWidget(),
                  title: lang.saleVouchersShort.toWidget(),
                  selected: selected == 3,
                  onTap: () => onChanged?.call(3),
                ),
                const Divider(),
              ],
              ListTile(
                leading: icons.branchOffice.toWidget(),
                title: lang.branchOffices.toWidget(),
                onTap: () => _pushRoute(context, building),
              ),
              ListTile(
                leading: icons.brand.toWidget(),
                title: lang.brands.toWidget(),
              ),
              ListTile(
                leading: icons.cashierBox.toWidget(),
                title: lang.cashierBoxes.toWidget(),
              ),
              ListTile(
                leading: icons.category.toWidget(),
                title: lang.categories.toWidget(),
              ),
              ListTile(
                leading: icons.discount.toWidget(),
                title: lang.discounts.toWidget(),
              ),
              const Divider(),
              const _DrawerSubtitle(text: 'Money'),
              ListTile(
                leading: icons.salePayment.toWidget(),
                title: lang.salePayments.toWidget(),
              ),
              ListTile(
                leading: icons.payment.toWidget(),
                title: lang.payments.toWidget(),
              ),
              const Divider(),
              const _DrawerSubtitle(text: 'Stock'),
              ListTile(
                leading: icons.stock.toWidget(),
                title: lang.stocks.toWidget(),
              ),
              ListTile(
                leading: icons.transfer.toWidget(),
                title: lang.transfers.toWidget(),
              ),
              const Divider(),
              const _DrawerSubtitle(text: 'People'),
              ListTile(
                leading: icons.user.toWidget(),
                title: lang.users.toWidget(),
              ),
              ListTile(
                leading: icons.phone.toWidget(),
                title: lang.phones.toWidget(),
              ),
              ListTile(
                leading: icons.payment.toWidget(),
                title: lang.contacts.toWidget(),
              ),
            ],
          ),
        );
      },
    );
  }

  Widget? _headerText(String? text, TextStyle? extendedStyle) {
    if (text != null) {
      return Text(text, style: extended ? extendedStyle : null);
    }
  }

  Widget? _accountCharacter(String? name) {
    if (name != null) {
      return CircleAvatar(child: Text(name.characters.first));
    }
  }

  void _pushRoute(BuildContext context, String routeName) {
    final navigator = Navigator.of(context);
    if (extended) {
      navigator.pushNamed(routeName);
    } else {
      navigator.popAndPushNamed(routeName);
    }
  }
}
