import 'dart:async';

import 'package:animations/animations.dart';
import 'package:beamer/beamer.dart';
import 'package:breakpoint/breakpoint.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:provider/provider.dart';
import 'package:stocker/configurations/constrants.dart';
import 'package:stocker/configurations/preferences.dart';
import 'package:stocker/extensions/widgets.dart';
import 'package:stocker/lang/l10n.dart';
import 'package:stocker/models/models.dart';
import 'package:stocker/repositories/repositories.dart';
import 'package:stocker/routes/routes.dart';
import 'package:stocker/services/api/api.dart';
import 'package:stocker/widgets/adaptive_modal.dart';
import 'package:stocker/widgets/animated_index_stack.dart';
import 'package:stocker/widgets/constrants.dart';
import 'package:stocker/widgets/providers/authentication.dart';
import 'package:stocker/widgets/providers/model_icon_data.dart';

part 'app_bar.dart';
part 'drawer.dart';
part 'logout_modal.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({
    Key? key,
    required this.initialIndex,
  }) : super(key: key);

  final int initialIndex;

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late int _selectedTab;

  @override
  void initState() {
    _selectedTab = widget.initialIndex != -1 ? widget.initialIndex : 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // Language
    final lang = Language.of(context);
    final langDashboard = lang.dashboard;
    final langProducts = lang.products;
    final langPurchases = lang.purchaseOrdersShort;
    final langSales = lang.saleVouchersShort;

    // Icons
    final icons = ModelIconData.of(context)!;
    const iconDashboard = Icon(Icons.dashboard);
    final iconProducts = Icon(icons.product);
    final iconPurchases = Icon(icons.purchaseOrder);
    final iconSales = Icon(icons.saleVoucher);

    return BreakpointBuilder(
      builder: (context, breakpoint) {
        final fab = FloatingActionButton(
          onPressed: () => {},
          child: const Icon(Icons.add),
        );

        final body = IndexedTransitionSwitcher(
          index: _selectedTab,
          transitionBuilder: (child, primary, secondary) {
            return FadeThroughTransition(
              animation: primary,
              secondaryAnimation: secondary,
              child: child,
            );
          },
          children: const [
            ContainerPlaceholder(name: 'Dashboard'),
            InjectorPlacholder(to: '/products'),
            InjectorPlacholder(to: '/purchases'),
            InjectorPlacholder(to: '/sales'),
          ],
        );

        final device = breakpoint.device;
        // Mobile Phone
        if (device <= LayoutClass.largeHandset) {
          return Scaffold(
            appBar: _HomeScreenAppBar(breakpoint: breakpoint),
            drawer: const _MainDrawer(extended: false),
            body: body,
            floatingActionButton: fab,
            bottomNavigationBar: BottomNavigationBar(
              type: BottomNavigationBarType.fixed,
              onTap: _onSelectTab,
              currentIndex: _selectedTab,
              items: [
                BottomNavigationBarItem(
                  icon: iconDashboard,
                  label: langDashboard,
                ),
                BottomNavigationBarItem(
                  icon: iconProducts,
                  label: langProducts,
                ),
                BottomNavigationBarItem(
                  icon: iconPurchases,
                  label: langPurchases,
                ),
                BottomNavigationBarItem(
                  icon: iconSales,
                  label: langSales,
                )
              ],
            ),
          );
        } else
        // Tablet
        if (device >= LayoutClass.smallTablet && device < LayoutClass.desktop) {
          return Scaffold(
            appBar: _HomeScreenAppBar(breakpoint: breakpoint),
            drawer: const _MainDrawer(extended: false),
            body: Row(
              children: [
                NavigationRail(
                  leading: fab,
                  destinations: [
                    NavigationRailDestination(
                      icon: iconDashboard,
                      label: Text(langDashboard),
                    ),
                    NavigationRailDestination(
                      icon: iconProducts,
                      label: Text(langProducts),
                    ),
                    NavigationRailDestination(
                      icon: iconPurchases,
                      label: Text(langPurchases),
                    ),
                    NavigationRailDestination(
                      icon: iconSales,
                      label: Text(langSales),
                    ),
                  ],
                  selectedIndex: _selectedTab,
                  onDestinationSelected: _onSelectTab,
                ),
                Expanded(child: body),
              ],
            ),
          );
        }
        // Desktop
        else {
          return Row(
            children: [
              _MainDrawer(
                extended: true,
                onChanged: _onSelectTab,
                selected: _selectedTab,
              ),
              const VerticalDivider(thickness: 1, width: 1),
              Expanded(
                child: Scaffold(
                  appBar: _HomeScreenAppBar(breakpoint: breakpoint),
                  floatingActionButton: fab,
                  floatingActionButtonLocation:
                      FloatingActionButtonLocation.endTop,
                  body: body,
                ),
              )
            ],
          );
        }
      },
    );
  }

  void _onSelectTab(int index) {
    final tab = homeTabs[index];
    final query = tab != null ? {homeTabQuery: tab} : null;

    final uri = Uri(
      path: '/',
      queryParameters: query,
    );

    Beamer.of(context).update(
      state: BeamState.fromUri(uri),
      rebuild: false,
    );

    setState(() {
      _selectedTab = index;
    });
  }
}

class ContainerPlaceholder extends StatefulWidget {
  const ContainerPlaceholder({
    Key? key,
    required this.name,
  }) : super(key: key);

  final String name;

  @override
  State<ContainerPlaceholder> createState() => _ContainerPlaceholderState();
}

class _ContainerPlaceholderState extends State<ContainerPlaceholder> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        const Placeholder(),
        Text(widget.name),
      ],
    );
  }
}

class InjectorPlacholder extends StatelessWidget {
  const InjectorPlacholder({
    Key? key,
    required this.to,
  }) : super(key: key);

  final String to;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ElevatedButton(
        onPressed: () => Beamer.of(context).beamToNamed(to),
        child: Text('See All ($to)'),
      ),
    );
  }
}

class _DrawerSubtitle extends StatelessWidget {
  const _DrawerSubtitle({
    Key? key,
    required this.text,
  }) : super(key: key);

  final String text;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: Text(text, style: Theme.of(context).textTheme.subtitle2),
    );
  }
}
