part of 'home_screen.dart';

class _HomeScreenAppBar extends StatelessWidget implements PreferredSizeWidget {
  const _HomeScreenAppBar({
    Key? key,
    required this.breakpoint,
  }) : super(key: key);

  final Breakpoint breakpoint;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return AppBar(
      title:
          breakpoint.device < LayoutClass.desktop ? const Text(kAppName) : null,
      bottom: breakpoint.device >= LayoutClass.desktop
          ? PreferredSize(
              preferredSize: appBarBottomSize,
              child: Container(
                alignment: AlignmentDirectional.centerStart,
                margin: appBarBottomMargin,
                child: Text(
                  kAppName,
                  style: theme.primaryTextTheme.headline6,
                ),
              ),
            )
          : null,
      actions: [
        IconButton(
          onPressed: () => _profileButtonClick(context),
          icon: const Icon(Icons.account_circle),
        ),
        IconButton(
          tooltip: Language.of(context).logout,
          onPressed: () => _logoutButtonClick(context),
          icon: const Icon(Icons.logout),
        ),
      ],
    );
  }

  @override
  Size get preferredSize => breakpoint.device >= LayoutClass.desktop
      ? appBarDesktopSize
      : appBarStandardSize;

  void _profileButtonClick(BuildContext context) {
    Beamer.of(context).beamToNamed('/profile');
  }

  void _logoutButtonClick(BuildContext context) {
    showCustomModalBottomSheet(
      context: context,
      builder: (context) => const LogoutModal(),
      containerWidget: (_, animation, child) => AdaptiveModal(child: child),
      expand: false,
    );
  }
}
