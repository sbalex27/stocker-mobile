part of 'home_screen.dart';

class LogoutModal extends StatefulWidget {
  const LogoutModal({Key? key}) : super(key: key);

  @override
  _LogoutModalState createState() => _LogoutModalState();
}

class _LogoutModalState extends State<LogoutModal> {
  bool _proccessing = false;
  String? _errorCaption;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;
    final lang = Language.of(context);
    return Selector<Authentication, User?>(
      selector: (context, env) => env.user,
      builder: (context, auth, child) => Container(
        padding: const EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: [
            // Title
            Text(
              lang.logout,
              style: textTheme.headline6,
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 24),

            // Account info
            if (auth != null)
              Column(
                children: [
                  CircleAvatar(child: Text(auth.firstname.characters.first)),
                  const SizedBox(height: 8),
                  Text(
                    auth.toString(),
                    textAlign: TextAlign.center,
                    style: textTheme.subtitle1,
                  ),
                  Text(
                    auth.email,
                    textAlign: TextAlign.center,
                  ),
                ],
              )
            else
              const Center(child: CircularProgressIndicator()),

            // Body
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 16),
              child: Text(
                lang.logoutWarning,
                textAlign: TextAlign.justify,
              ),
            ),

            if (_errorCaption != null)
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8),
                child: Text(
                  _errorCaption!,
                  style: TextStyle(color: theme.errorColor),
                ),
              ),

            // Button
            if (_proccessing)
              const Align(
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 8.0),
                  child: CircularProgressIndicator.adaptive(),
                ),
              )
            else
              ElevatedButton(
                onPressed: _continueClick,
                child: Text(lang.carryOn.toUpperCase()),
              ),
          ],
        ),
      ),
    );
  }

  void _continueClick() {
    setState(() => _proccessing = true);

    context
        .read<AuthApi>()
        .logout()
        .timeout(const Duration(minutes: 1))
        .then(_handle)
        .onError(
          _handleUnsuccessful,
          test: (e) => e is UnsuccessfulException,
        )
        .onError(
          _handleUnauthorized,
          test: (e) => e is UnauthorizedException,
        )
        .whenComplete(_whenCompleteLogout);
  }

  void _whenCompleteLogout() {
    setState(() {
      _proccessing = false;
    });
  }

  void _handleUnsuccessful(UnsuccessfulException error, StackTrace _) {
    setState(() {
      _errorCaption = error.message;
    });
  }

  void _handleUnauthorized(UnauthorizedException error, StackTrace _) {
    Token().remove().then((removed) {
      context.read<Authentication>().token = null;
      Beamer.of(context).popToNamed('/login');
    });
  }

  Future<void> _handle(User response) {
    return Token().remove().then((removed) {
      if (removed) {
        context.read<Authentication>().token = null;
        Navigator.of(context)
            .pushNamedAndRemoveUntil(login, (route) => route.isFirst);
      }
    });
  }
}
