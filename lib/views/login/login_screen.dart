import 'package:beamer/beamer.dart';
import 'package:breakpoint/breakpoint.dart';
import 'package:collection/collection.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:stocker/configurations/preferences.dart';
import 'package:stocker/lang/l10n.dart';
import 'package:stocker/models/models.dart';
import 'package:stocker/repositories/repositories.dart';
import 'package:stocker/services/api/api.dart';
import 'package:stocker/widgets/providers/authentication.dart';

part 'login_form.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  // Finals
  final _formState = GlobalKey<FormState>();
  final _credentials = Auth();

  // Vars
  bool _processing = false;
  List<FieldError>? _formFieldExceptions;

  @override
  Widget build(BuildContext context) {
    const logoPath = 'assets/images/logo_light.png';
    final form = LoginForm(
      formState: _formState,
      credentials: _credentials,
      fieldExceptions: _formFieldExceptions,
      onComplete: _processing ? null : _loginPressed,
    );

    final loginButton = AbsorbPointer(
      absorbing: _processing,
      child: ElevatedButton(
        onPressed: _loginPressed,
        child: Text(Language.of(context).login.toUpperCase()),
      ),
    );

    final handset = Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 64.0),
          child: Image.asset(logoPath, width: 100),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: form,
        ),
        ButtonBar(children: [loginButton]),
        if (_processing)
          const Expanded(
            child: Align(
              alignment: Alignment.bottomCenter,
              child: LinearProgressIndicator(),
            ),
          ),
      ],
    );

    final desktop = Stack(
      children: [
        Center(
          child: SizedBox(
            width: 500,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  children: [
                    Image.asset(logoPath, height: 100),
                    const SizedBox(width: 32),
                    Expanded(child: form)
                  ],
                ),
                const SizedBox(height: 8),
                ButtonBar(
                  buttonPadding: EdgeInsets.zero,
                  children: [loginButton],
                )
              ],
            ),
          ),
        ),
        if (_processing)
          const Align(
            alignment: Alignment(0, 0.5),
            child: CircularProgressIndicator(),
          )
      ],
    );

    return BreakpointBuilder(
      builder: (context, breakpoints) {
        return Scaffold(
          body: SafeArea(
            child: breakpoints.device <= LayoutClass.largeHandset
                ? handset
                : desktop,
          ),
        );
      },
    );
  }

  void _loginPressed() {
    final form = _formState.currentState!;
    if (form.validate()) {
      form.save();
      setState(() => _processing = true);
      _performRequest();
    }
  }

  void _performRequest() {
    context
        .read<AuthApi>()
        .login(_credentials)
        .timeout(const Duration(minutes: 1))
        .then(_handle)
        .onError(_onUnsuccessful, test: (e) => e is UnsuccessfulException)
        .onError((error, stackTrace) => _showErrorAlert(error))
        .whenComplete(() => setState(() => _processing = false));
  }

  Future<void> _handle(AuthResponse auth) {
    return Future.wait([
      Token().set(auth.token),
      Authenticated().set(auth.user),
    ]).then((success) => success.every((element) => true)).then((success) {
      context.read<Authentication>().user = auth.user;
      context.read<Authentication>().token = auth.token;
      Beamer.of(context).beamToNamed('/', replaceCurrent: true);
    });
  }

  void _onUnsuccessful(UnsuccessfulException e, StackTrace stackTrace) {
    ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(content: Text(e.toString())));
    setState(() {
      _formFieldExceptions = e.errors;
    });
  }

  void _showErrorAlert(Object? error) {
    final lang = Language.of(context);
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text(lang.errorTitle),
          content: Text(lang.errorCaptionRetry),
          actions: [
            TextButton(
              onPressed: () => Navigator.of(context).pop(),
              child: Text(lang.ok),
            )
          ],
        );
      },
    );
  }
}
