part of 'login_screen.dart';

class LoginForm extends StatefulWidget {
  const LoginForm({
    Key? key,
    required this.formState,
    required this.credentials,
    this.fieldExceptions,
    this.onComplete,
  }) : super(key: key);

  final GlobalKey<FormState> formState;
  final Auth credentials;
  final List<FieldError>? fieldExceptions;
  final VoidCallback? onComplete;

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  bool _obscurePassword = true;

  @override
  Widget build(BuildContext context) {
    final lang = Language.of(context);

    return Form(
      key: widget.formState,
      child: Theme(
        data: Theme.of(context).copyWith(
          inputDecorationTheme: const InputDecorationTheme(
            border: OutlineInputBorder(),
          ),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            TextFormField(
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                labelText: lang.email,
                errorText: widget.fieldExceptions
                    ?.firstWhereOrNull((element) => element.field == 'email')
                    ?.display(),
              ),
              onSaved: (text) => widget.credentials.email = text,
              textInputAction: TextInputAction.next,
              validator: (text) => text!.isEmpty
                  ? lang.requiredField(lang.email.toLowerCase())
                  : null,
            ),
            const SizedBox(height: 12),
            TextFormField(
              keyboardType: TextInputType.visiblePassword,
              decoration: InputDecoration(
                labelText: lang.password,
                errorText: widget.fieldExceptions
                    ?.firstWhereOrNull((element) => element.field == 'password')
                    ?.display(),
                suffixIcon: IconButton(
                  onPressed: _togglePasswordVisibility,
                  icon: Icon(
                    _obscurePassword ? Icons.visibility : Icons.visibility_off,
                  ),
                ),
              ),
              obscureText: _obscurePassword,
              onSaved: (text) => widget.credentials.password = text,
              textInputAction: TextInputAction.done,
              onEditingComplete: () {
                FocusScope.of(context).unfocus();
                widget.onComplete?.call();
              },
              validator: (text) => text!.isEmpty
                  ? lang.requiredField(lang.password.toLowerCase())
                  : null,
            ),
          ],
        ),
      ),
    );
  }

  void _togglePasswordVisibility() {
    setState(() {
      _obscurePassword = !_obscurePassword;
    });
  }
}
