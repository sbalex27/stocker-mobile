import 'package:breakpoint/breakpoint.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class InformativeContainer extends StatelessWidget {
  const InformativeContainer({
    Key? key,
    required this.asset,
    required this.title,
    required this.caption,
    required this.buttons,
  }) : super(key: key);

  final String asset;
  final String title;
  final String caption;
  final List<Widget> buttons;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context).textTheme;

    return ButtonBarTheme(
      data: const ButtonBarThemeData(
        buttonPadding: EdgeInsets.zero,
      ),
      child: BreakpointBuilder(
        builder: (context, breakpoint) {
          // Layout
          if (breakpoint.device >= LayoutClass.desktop) {
            return Padding(
              padding: const EdgeInsets.all(12.0),
              child: Center(
                child: Row(
                  children: [
                    Expanded(
                      child: SvgPicture.asset(asset, width: 500),
                    ),
                    const SizedBox(width: 50),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text(title, style: theme.headline3),
                          const SizedBox(height: 8),
                          Text(caption),
                          const SizedBox(height: 16),
                          ButtonBar(
                            alignment: MainAxisAlignment.start,
                            children: buttons,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          } else {
            return Container(
              margin: const EdgeInsets.all(16),
              child: Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SvgPicture.asset(asset, width: 200),
                    const SizedBox(height: 16),
                    Text(title, style: theme.headline6),
                    const SizedBox(height: 8),
                    Text(caption, textAlign: TextAlign.center),
                    const SizedBox(height: 16),
                    ButtonBar(
                      alignment: MainAxisAlignment.center,
                      children: buttons,
                    )
                  ],
                ),
              ),
            );
          }
        },
      ),
    );
  }
}
