import 'package:breakpoint/breakpoint.dart';
import 'package:flutter/material.dart';

class AdaptiveModal extends StatelessWidget {
  final Widget child;

  const AdaptiveModal({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BreakpointBuilder(
      builder: (context, breakpoint) =>
          breakpoint.device > LayoutClass.largeHandset
              ? SafeArea(
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Padding(
                      padding: const EdgeInsets.only(bottom: 12),
                      child: Material(
                        clipBehavior: Clip.antiAlias,
                        borderRadius: BorderRadius.circular(6),
                        child: SizedBox(
                          width: 400,
                          child: child,
                        ),
                      ),
                    ),
                  ),
                )
              : Material(child: child),
    );
  }
}
