import 'package:flutter/material.dart';
import 'package:stocker/configurations/preferences.dart';
import 'package:stocker/models/models.dart';

class Authentication with ChangeNotifier {
  User? _user;
  String? _token;

  User? get user => _user;
  String? get token => _token;

  set user(User? value) {
    _user = value;
    notifyListeners();
  }

  set token(String? value) {
    _token = value;
    notifyListeners();
  }

  bool get isAuthenticated => _token != null;

  Authentication({String? token, User? user})
      : _user = user,
        _token = token;

  factory Authentication.load() {
    return Authentication()..load();
  }

  Future<void> load() async {
    return Future.wait([
      Authenticated().get(),
      Token().get(),
    ]).then((value) {
      user = value[0] as User?;
      token = value[1] as String?;
    });
  }
}
