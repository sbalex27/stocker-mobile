import 'package:flutter/material.dart';

class ModelIconData extends InheritedWidget {
  const ModelIconData({
    Key? key,
    required Widget child,
    this.branchOffice = Icons.store,
    this.brand = Icons.label_important,
    this.cashierBox = Icons.attach_money,
    this.category = Icons.category,
    this.contact = Icons.contact_phone,
    this.discount = Icons.local_offer,
    this.payment = Icons.payments,
    this.phone = Icons.phone,
    this.product = Icons.inventory_2,
    this.purchaseOrder = Icons.shopping_cart,
    this.reference = Icons.connect_without_contact,
    this.salePayment = Icons.price_check,
    this.saleVoucher = Icons.receipt,
    this.stock = Icons.list_alt,
    this.transfer = Icons.local_shipping,
    this.user = Icons.person,
    this.log = Icons.history,
  }) : super(key: key, child: child);

  final IconData branchOffice;
  final IconData brand;
  final IconData cashierBox;
  final IconData category;
  final IconData contact;
  final IconData discount;
  final IconData payment;
  final IconData phone;
  final IconData product;
  final IconData purchaseOrder;
  final IconData reference;
  final IconData salePayment;
  final IconData saleVoucher;
  final IconData stock;
  final IconData transfer;
  final IconData user;
  final IconData log;

  static ModelIconData? of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<ModelIconData>();
  }

  @override
  bool updateShouldNotify(ModelIconData oldWidget) {
    return true;
  }
}
