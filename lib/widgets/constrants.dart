import 'package:flutter/material.dart';
import 'package:stocker/configurations/constrants.dart';

// Margins
const appBarBottomMargin = EdgeInsetsDirectional.only(
  start: kDesktopContentMargin,
  bottom: kAppBarBottomMargin,
);

// Sizes
const appBarBottomSize = Size.fromHeight(kAppBarBottomSize);
const appBarStandardSize = Size.fromHeight(kToolbarHeight);
const appBarDesktopSize = Size.fromHeight(kDesktopToolbarHeight);
