import 'package:flutter/widgets.dart';

typedef IsSetBuilder<T> = Widget Function(T value);

mixin IsSetMixin {
  Widget? isSet<T>({required T? value, required Widget then}) {
    if (value != null) {
      return then;
    }
  }

  Widget? isSetBuilder<T>({required T? value, required IsSetBuilder<T> builder}) {
    if (value != null) {
      return builder(value);
    }
  }
}
