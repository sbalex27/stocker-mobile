// Global
const kAppName = 'Stocker';
const kWSDomain = '147.182.138.14';

// Layout Sizes
const double kDesktopToolbarHeight = 128;
const double kAppBarBottomSize = 56;
const double kDesktopContentMargin = 72;
const double kAppBarBottomMargin = 22;
