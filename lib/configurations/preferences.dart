import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:stocker/models/models.dart';

abstract class StorablePreference<T> {
  /// Get the shared preference instance
  Future<SharedPreferences> get _preferences => SharedPreferences.getInstance();

  /// The key value pair for find the preference
  String get key;

  /// Set a new value or override a existent value
  Future<bool> set(T value);

  /// Get the stored value
  Future<T?> get();

  /// Remove the value, the equivalent of set `null`
  Future<bool> remove();
}

class Token extends StorablePreference<String> {
  static final Token _auth = Token._internal();

  factory Token() {
    return _auth;
  }

  Token._internal();

  @override
  Future<String?> get() {
    return _preferences.then((instance) => instance.getString(key));
  }

  @override
  String get key => 'jwt';

  @override
  Future<bool> remove() {
    return _preferences.then((instance) => instance.remove(key));
  }

  @override
  Future<bool> set(String value) {
    return _preferences.then((instance) => instance.setString(key, value));
  }
}

class Authenticated extends StorablePreference<User> {
  static final Authenticated _authenticated = Authenticated._internal();

  factory Authenticated() {
    return _authenticated;
  }

  Authenticated._internal();

  @override
  Future<User?> get() {
    return _preferences
        .then((instance) => instance.getString(key))
        .then((value) => value != null ? jsonDecode(value) as Map<String, dynamic> : null)
        .then((value) => value != null ? User.fromJson(value) : null);
  }

  @override
  String get key => 'authenticated';

  @override
  Future<bool> remove() {
    return _preferences.then((instance) => instance.remove(key));
  }

  @override
  Future<bool> set(User value) {
    final String encoded = jsonEncode(value.toJson());
    return _preferences.then((instance) => instance.setString(key, encoded));
  }
}
