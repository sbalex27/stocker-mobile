part of api;

class ProductsApi extends BaseRestful<Product>
    with StoreMixin, UpdateMixin, DeleteMixin, IndexableMixin
    implements ProductRepository {
  ProductsApi(StockerClient client) : super(client);

  @override
  ResourceRouter get router => ResourceRouter(parts: ['products']);

  @override
  Future<Product> show(int id, {bool detailed = false}) {
    final uri = router.resolve([id]).replace(
      queryParameters: {
        'detailed': detailed.toString(),
      },
    );

    return client.get(uri).then(_resolve);
  }
}

class BrandsApi extends Restful<Brand> implements BrandsRepository {
  BrandsApi(StockerClient client) : super(client);

  @override
  ResourceRouter get router => ResourceRouter(parts: ['brands']);
}

class CategoriesApi extends Restful<Category> implements CategoryRepository {
  CategoriesApi(StockerClient client) : super(client);

  @override
  ResourceRouter get router => ResourceRouter(parts: ['categories']);
}
