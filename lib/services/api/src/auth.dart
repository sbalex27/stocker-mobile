part of api;

class AuthApi extends WebService {
  AuthApi(StockerClient client) : super(client);

  /// Login and return the JavaScript Object Notation Web Token (JWT)
  Future<AuthResponse> login(Serializable credentials) {
    final uri = Uri.http(kWSDomain, '/api/auth/login');
    return client
        .post(uri, body: credentials.toJson())
        .then((value) => Resolver<AuthResponse>(value).decode());
  }

  /// Returns the authenticated user from the
  /// JavaScript Object Notation Web Token
  Future<User> logged() {
    return client
        .get(Uri.http(kWSDomain, '/api/auth/logged'))
        .then((value) => Resolver<User>(value).decode());
  }

  /// Logout the user and revoke the token in the backend
  Future<User> logout() {
    return client
        .post(Uri.http(kWSDomain, '/api/auth/logout'))
        .then((value) => Resolver<User>(value).decode());
  }
}
