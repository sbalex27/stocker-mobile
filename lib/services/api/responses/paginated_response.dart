part of api;

@JsonSerializable()
class ApiPaginatedResponse<T>
    implements StandarizedResponse<PaginatedResource<T>>, Serializable {
  @override
  List<FieldException>? errors;

  @override
  String? message;

  @override
  PaginatedResource<T>? payload;

  @override
  bool success;

  ApiPaginatedResponse({
    required this.success,
    this.payload,
    this.message,
    this.errors,
  });

  factory ApiPaginatedResponse.fromJson(Map<String, dynamic> json) {
    return _$ApiPaginatedResponseFromJson(json);
  }

  @override
  Map<String, dynamic> toJson() {
    return _$ApiPaginatedResponseToJson(this);
  }
}
