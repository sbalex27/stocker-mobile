part of api;

@JsonSerializable()
class PaginatedResource<T> implements Paginated<T>, Serializable {
  @override
  int currentPage;
  @override
  @_ModelTypeMapper()
  List<T> data;
  Uri firstPageUrl;
  @override
  int from;
  @override
  int lastPage;
  Uri lastPageUrl;
  Uri? nextPageUrl;
  Uri path;
  @override
  int perPage;
  Uri? prevPageUrl;
  @override
  int to;
  @override
  int total;

  PaginatedResource({
    required this.currentPage,
    required this.data,
    required this.firstPageUrl,
    required this.from,
    required this.lastPage,
    required this.lastPageUrl,
    required this.path,
    required this.perPage,
    required this.to,
    required this.total,
    this.nextPageUrl,
    this.prevPageUrl,
  });

  factory PaginatedResource.fromJson(Map<String, dynamic> json) =>
      _$PaginatedResourceFromJson(json);

  @override
  Map<String, dynamic> toJson() {
    return _$PaginatedResourceToJson(this);
  }
}
