part of api;

@JsonSerializable()
class FieldException extends Equatable implements FieldError, Serializable {
  @override
  final String field;

  @override
  final List<String> errors;

  const FieldException(this.field, this.errors);

  factory FieldException.fromJson(Map<String, dynamic> json) =>
      _$FieldExceptionFromJson(json);

  @override
  List<Object?> get props => [field, errors];

  @override
  Map<String, dynamic> toJson() {
    return _$FieldExceptionToJson(this);
  }

  @override
  String display() {
    return errors.join(". ");
  }
}
