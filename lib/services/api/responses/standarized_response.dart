part of api;

/// Use as a interface to implements the required properties
/// to the children.
/// 
/// **Do:** 
/// 
/// Implements this interface to responses with nested resources
/// like api responses, paginated responses or massive serializations.
/// Example:
/// 
/// ```
/// class PaginatedResponse<T> implements 
///       IStandarizedResponse<PaginatedResponse<T>> {}
/// ```
/// 
/// **Don't:** 
/// 
/// Implements this interface to specific resource or 
/// make an response for each model.
/// Example:
/// 
/// ```
/// class BrandResponse implements IStandarizedResponse<Brand> {}
/// ```
/// 
/// Insthead, use the generic class [ApiResponse]:
/// 
/// ```
/// ApiResponse<Brand>.fromJson(json);
/// ```
abstract class StandarizedResponse<T> {
  /// `true` if the server return an success response
  late bool success;
  /// Main data response
  T? payload;
  /// Response message, can be success, warning or error
  String? message;
  /// Errors collection if the request was a form request and the response
  /// contains field errors
  List<FieldException>? errors;
}
