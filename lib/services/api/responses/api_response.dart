part of api;

@JsonSerializable()
class ApiResponse<T> implements StandarizedResponse<T>, Serializable {
  @override
  List<FieldException>? errors;

  @override
  String? message;

  @override
  @_ModelTypeMapper()
  T? payload;

  @override
  bool success;

  ApiResponse({
    this.payload,
    this.message,
    this.errors,
    required this.success,
  });

  factory ApiResponse.fromJson(Map<String, dynamic> json) {
    return _$ApiResponseFromJson(json);
  }

  @override
  Map<String, dynamic> toJson() {
    return _$ApiResponseToJson(this);
  }
}
