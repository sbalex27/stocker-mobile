part of api;

mixin Decoder<T> {
  T _resolve(http.Response response) {
    // Server errors
    _thrower(response.statusCode);

    // Decode
    final json = _decoder(response.body);

    // Deserialize
    final StandarizedResponse<T> deserialized = ApiResponse<T>.fromJson(json);

    // Debug Print
    debugPrint('✔️ Recived from ${response.request?.url}');
    dev.inspect(deserialized);

    // Validate
    return _extractPayload(deserialized);
  }

  Map<String, dynamic> _decoder(String value) {
    return jsonDecode(value) as Map<String, dynamic>;
  }

  /// Analyze the response status [code], if is a error code
  /// then throws an [RepositoryException]
  void _thrower(int code) {
    switch (code) {
      case HttpStatus.internalServerError:
        throw const InternalException();
      case HttpStatus.unauthorized:
        throw const UnauthorizedException();
      default:
        return;
    }
  }

  /// Extracts and return the response payload from the [deserialized],
  /// if [deserialized.success] is false then throws an [UnsuccessfulException]
  O _extractPayload<O>(StandarizedResponse<O> deserialized) {
    return deserialized.success
        ? deserialized.payload!
        : throw UnsuccessfulException(
            message: deserialized.message!,
            errors: deserialized.errors!,
          );
  }
}

class Resolver<T> with Decoder<T> {
  const Resolver(this.response);

  final http.Response response;

  T decode() => _resolve(response);
}
