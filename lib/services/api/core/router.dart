part of api;

/// Its main purpose is the creation of a [Uri] object using
/// the `authority`, `parts` and `bindings` properties
/// that will contain a valid URL for the consumption
/// of an api resource using the [resolve] method.
///
/// Example:
/// ```
/// ResourceRouter(
///   authority: 'stocker.com',
///   parts: ['branch-offices', 'products']
/// ).resolve([2,4]).toString();
/// ```
///
/// Will return http://stocker.com/branch-offices/2/products/4
class ResourceRouter {
  /// URL domain and subdomain
  String get _authority => kWSDomain;

  /// Parts used in the path. It can't be empty, it will contain at least one
  final List<String> parts;

  const ResourceRouter({
    required this.parts,
  }) : assert(parts.length >= 1);

  /// Resolve the [Uri] object based in the constructor attributes
  /// `authority` and `parts`. The optional `bindings` list parameter
  /// pass the chill and parent route params, this parameter will be
  /// at the same lenght or one less than parts attribute.
  ///
  /// ```
  /// _resourceRouter.resolve([1]); // .../child/1
  /// _resourceRouter.resolve([1, 2]); // .../parent/1/child/2
  /// ```
  ///
  /// If you need replace or add query parameters you can
  /// use the `replace` method:
  ///
  /// ```
  /// _resourceRouter.resolve().replace(
  ///   queryParameters: {'page': 1}
  /// );
  /// ```
  Uri resolve([List bindings = const []]) {
    final paths = parts.map<SlicedPath>((e) {
      final partIndex = parts.indexOf(e);
      final id = bindings.length - 1 < partIndex ? null : bindings[partIndex];
      return SlicedPath(e, id);
    }).toList();

    return Uri.http(_authority, "api${_resolvePath(paths)}");
  }

  String _resolvePath(List<SlicedPath> silicedPaths) {
    final resolved = StringBuffer();
    for (final item in silicedPaths) {
      resolved.write(item.toString());
    }
    return resolved.toString();
  }
}

/// Make and parse an sliced Uri path with the method `toString()`.
/// Example:
///
/// ```
/// SlicedPath('contacts').toString(); // /contacts
/// SlicedPath('phones', 1).toString(); // /phones/1
/// ```
class SlicedPath {
  /// The path accessor
  final String accessor;

  /// The model attribute binding
  final dynamic id;

  const SlicedPath(this.accessor, [this.id]);

  @override
  String toString() {
    var path = "/$accessor";
    if (id != null) {
      path += "/$id";
    }
    return path;
  }
}
