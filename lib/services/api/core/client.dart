part of api;

/// Stocker http client
///
/// The Stocker client inherits from [http.BaseClient]
/// to implement the headers required by the server
///
/// ```
/// StockerClient({
///   token: _token,
///   locale: 'en',
///   inner: http.Client(),
/// }).post(_uri, _data);
/// ```
class StockerClient extends http.BaseClient {
  /// Auth client token
  final String? token;

  /// Location header for server responses
  final String lang;

  // Inner http client
  final http.Client inner;

  StockerClient(
    this.token, {
    required this.lang,
    required this.inner,
  });

  @override
  Future<http.StreamedResponse> send(http.BaseRequest request) {
    if (token != null) {
      request.headers[HttpHeaders.authorizationHeader] = 'Bearer ${token!}';
    }

    request.headers[HttpHeaders.contentLanguageHeader] = lang;
    request.headers[HttpHeaders.acceptHeader] = 'application/json';

    debugPrint('☁️ Requested to: ${request.url}');
    return inner.send(request);
  }

  @override
  void close() {
    inner.close();
  }
}
