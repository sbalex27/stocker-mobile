part of api;

/// Inherit the [StockerClient] to make [http.Request]
///
/// **Do:**
///
/// Extends the abstract class
/// ```
/// class ExampleApi extends WebService {
///   const ExampleApi(StockerClient client) : super(client)
/// }
/// ```
///
/// **Don't:**
///
/// Use as interface
/// ```
/// class ExampleApi implements WebService {
///   @override
///   StockerClient client;
/// }
/// ```
abstract class WebService {
  const WebService(this.client);

  /// API main client, implements all the required headers
  /// and parameters to make an request
  final StockerClient client;
}
