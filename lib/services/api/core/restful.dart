part of api;

abstract class BaseRestful<T extends Model> extends WebService with Decoder<T> {
  const BaseRestful(StockerClient client) : super(client);

  ResourceRouter get router;
}

abstract class Restful<T extends Model> extends BaseRestful<T>
    with
        StoreMixin<T>,
        UpdateMixin<T>,
        ShowMixin<T>,
        DeleteMixin<T>,
        IndexableMixin<T> {
  Restful(StockerClient client) : super(client);
}
