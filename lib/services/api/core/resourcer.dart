part of api;

mixin StoreMixin<T extends Model> on BaseRestful<T> implements Storable<T> {
  @override
  Future<T> store(Serializable model) {
    return client.post(router.resolve(), body: model.toJson()).then(_resolve);
  }
}

mixin UpdateMixin<T extends Model> on BaseRestful<T> implements Editable<T> {
  @override
  Future<T> update(int id, Serializable model) {
    return client
        .put(router.resolve([id]), body: model.toJson())
        .then(_resolve);
  }
}

mixin ShowMixin<T extends Model> on BaseRestful<T> implements Showable<T> {
  @override
  Future<T> show(int id) {
    return client.get(router.resolve([id])).then(_resolve);
  }
}

mixin DeleteMixin<T extends Model> on BaseRestful<T> implements Removable<T> {
  @override
  Future<T> delete(Identifiable model) {
    return client.delete(router.resolve([model.id])).then(_resolve);
  }
}

mixin IndexableMixin<T extends Model> on BaseRestful<T> implements Pageable<T> {
  @override
  Future<Paginated<T>> index(int page) {
    final uri = router.resolve().replace(
      queryParameters: {'page': page.toString()},
    );

    return client
        .get(uri)
        .then((response) {
          _thrower(response.statusCode);
          return _decoder(response.body);
        })
        .then((value) => ApiPaginatedResponse<T>.fromJson(value))
        .then(_extractPayload);
  }
}
