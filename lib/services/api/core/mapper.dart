part of api;

class _ModelTypeMapper<T> implements JsonConverter<T, Map<String, dynamic>?> {
  const _ModelTypeMapper();

  bool _equalsT<T1, T2>() => T == T1 || T == T2;

  @override
  T fromJson(Map<String, dynamic>? json) {
    if (json == null) {
      return json as T;
    } else if (_equalsT<Product, Product?>()) {
      return Product.fromJson(json) as T;
    } else if (_equalsT<User, User?>()) {
      return User.fromJson(json) as T;
    } else if (_equalsT<Brand, Brand?>()) {
      return Brand.fromJson(json) as T;
    } else if (_equalsT<PaginatedResource, PaginatedResource?>()) {
      return PaginatedResource.fromJson(json) as T;
    } else if (_equalsT<AuthResponse, AuthResponse?>()) {
      return AuthResponse.fromJson(json) as T;
    } else {
      return json as T;
    }
  }

  @override
  Map<String, dynamic>? toJson(T object) {
    if (object is Serializable) {
      return object.toJson();
    }
  }
}
