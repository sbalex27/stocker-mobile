// GENERATED CODE - DO NOT MODIFY BY HAND

part of api;

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ApiResponse<T> _$ApiResponseFromJson<T>(Map<String, dynamic> json) {
  return ApiResponse<T>(
    payload: _ModelTypeMapper<T?>()
        .fromJson(json['payload'] as Map<String, dynamic>?),
    message: json['message'] as String?,
    errors: (json['errors'] as List<dynamic>?)
        ?.map((e) => FieldException.fromJson(e as Map<String, dynamic>))
        .toList(),
    success: json['success'] as bool,
  );
}

Map<String, dynamic> _$ApiResponseToJson<T>(ApiResponse<T> instance) =>
    <String, dynamic>{
      'errors': instance.errors,
      'message': instance.message,
      'payload': _ModelTypeMapper<T?>().toJson(instance.payload),
      'success': instance.success,
    };

AuthResponse _$AuthResponseFromJson(Map<String, dynamic> json) {
  return AuthResponse(
    user: User.fromJson(json['user'] as Map<String, dynamic>),
    token: json['token'] as String,
  );
}

FieldException _$FieldExceptionFromJson(Map<String, dynamic> json) {
  return FieldException(
    json['field'] as String,
    (json['errors'] as List<dynamic>).map((e) => e as String).toList(),
  );
}

Map<String, dynamic> _$FieldExceptionToJson(FieldException instance) =>
    <String, dynamic>{
      'field': instance.field,
      'errors': instance.errors,
    };

PaginatedResource<T> _$PaginatedResourceFromJson<T>(Map<String, dynamic> json) {
  return PaginatedResource<T>(
    currentPage: json['current_page'] as int,
    data: (json['data'] as List<dynamic>)
        .map((e) => _ModelTypeMapper<T>().fromJson(e as Map<String, dynamic>?))
        .toList(),
    firstPageUrl: Uri.parse(json['first_page_url'] as String),
    from: json['from'] as int,
    lastPage: json['last_page'] as int,
    lastPageUrl: Uri.parse(json['last_page_url'] as String),
    path: Uri.parse(json['path'] as String),
    perPage: json['per_page'] as int,
    to: json['to'] as int,
    total: json['total'] as int,
    nextPageUrl: json['next_page_url'] == null
        ? null
        : Uri.parse(json['next_page_url'] as String),
    prevPageUrl: json['prev_page_url'] == null
        ? null
        : Uri.parse(json['prev_page_url'] as String),
  );
}

Map<String, dynamic> _$PaginatedResourceToJson<T>(
        PaginatedResource<T> instance) =>
    <String, dynamic>{
      'current_page': instance.currentPage,
      'data': instance.data.map(_ModelTypeMapper<T>().toJson).toList(),
      'first_page_url': instance.firstPageUrl.toString(),
      'from': instance.from,
      'last_page': instance.lastPage,
      'last_page_url': instance.lastPageUrl.toString(),
      'next_page_url': instance.nextPageUrl?.toString(),
      'path': instance.path.toString(),
      'per_page': instance.perPage,
      'prev_page_url': instance.prevPageUrl?.toString(),
      'to': instance.to,
      'total': instance.total,
    };

ApiPaginatedResponse<T> _$ApiPaginatedResponseFromJson<T>(
    Map<String, dynamic> json) {
  return ApiPaginatedResponse<T>(
    success: json['success'] as bool,
    payload: json['payload'] == null
        ? null
        : PaginatedResource.fromJson(json['payload'] as Map<String, dynamic>),
    message: json['message'] as String?,
    errors: (json['errors'] as List<dynamic>?)
        ?.map((e) => FieldException.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$ApiPaginatedResponseToJson<T>(
        ApiPaginatedResponse<T> instance) =>
    <String, dynamic>{
      'errors': instance.errors,
      'message': instance.message,
      'payload': instance.payload,
      'success': instance.success,
    };
