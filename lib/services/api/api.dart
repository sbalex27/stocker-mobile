library api;

import 'dart:convert';
import 'dart:developer' as dev;
import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:http/http.dart' as http;
import 'package:json_annotation/json_annotation.dart';
import 'package:stocker/configurations/constrants.dart';
import 'package:stocker/models/models.dart';
import 'package:stocker/repositories/repositories.dart';

part 'api.g.dart';
part 'core/client.dart';
part 'core/decoder.dart';
part 'core/mapper.dart';
part 'core/resourcer.dart';
part 'core/restful.dart';
part 'core/router.dart';
part 'core/service.dart';
part 'responses/api_response.dart';
part 'responses/auth_response.dart';
part 'responses/field_exception.dart';
part 'responses/paginated_resource.dart';
part 'responses/paginated_response.dart';
part 'responses/standarized_response.dart';
part 'src/api_resources.dart';
part 'src/auth.dart';
