import 'package:beamer/beamer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:stocker/configurations/constrants.dart';
import 'package:stocker/lang/l10n.dart';
import 'package:stocker/repositories/repositories.dart';
import 'package:stocker/routes/routes.dart' as router;
import 'package:stocker/services/api/api.dart';
import 'package:stocker/widgets/providers/authentication.dart';
import 'package:stocker/widgets/providers/model_icon_data.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  final auth = Authentication();
  await auth.load();

  runApp(
    StockerApp(
      initialRoute: '/',
      providers: [
        ChangeNotifierProvider<Authentication>.value(
          value: auth,
        ),
        ProxyProvider<Authentication, StockerClient>(
          update: (context, auth, ancestor) => StockerClient(
            auth.token,
            lang: Localizations.localeOf(context).languageCode,
            inner: Client(),
          ),
        ),
        ProxyProvider<StockerClient, AuthApi>(
          update: (context, client, ancestor) => AuthApi(client),
        ),
        ProxyProvider<StockerClient, ProductRepository>(
          update: (context, client, ancestor) => ProductsApi(client),
        ),
      ],
    ),
  );
}

class StockerApp extends StatelessWidget {
  StockerApp({
    Key? key,
    required this.providers,
    required this.initialRoute,
  })  : _routerDelegate = router.buildDelegate(initialRoute),
        super(key: key);

  final List<SingleChildWidget> providers;
  final String initialRoute;

  final BeamerDelegate<BeamState> _routerDelegate;

  @override
  Widget build(BuildContext context) {
    // App
    const railTheme = NavigationRailThemeData(
      elevation: 2,
      labelType: NavigationRailLabelType.selected,
    );

    return ModelIconData(
      child: MaterialApp.router(
        routeInformationParser: BeamerParser(),
        routerDelegate: _routerDelegate,
        backButtonDispatcher: BeamerBackButtonDispatcher(
          delegate: _routerDelegate,
        ),
        title: kAppName,
        theme: ThemeData(
          colorScheme: const ColorScheme.light(
            primary: Colors.teal,
            secondary: Colors.yellow,
          ),
          navigationRailTheme: railTheme,
        ),
        darkTheme: ThemeData(
          colorScheme: ColorScheme.fromSwatch(
            primarySwatch: Colors.teal,
            accentColor: Colors.tealAccent,
            brightness: Brightness.dark,
          ),
          navigationRailTheme: railTheme,
        ),
        builder: (context, child) {
          return MultiProvider(
            providers: providers,
            child: child,
          );
        },
        localizationsDelegates: const [
          Language.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: Language.delegate.supportedLocales,
      ),
    );
  }
}
