part of repositories;

/// Model a field error for an service
abstract class FieldError {
  final String field;
  final List<String> errors;

  const FieldError(this.field, this.errors);

  /// Display errors
  String display();
}

/// Model the meta for a paginated service response for
/// an resource of type `T`
abstract class Paginated<T> {
  late List<T> data;
  late int from;
  late int to;
  late int currentPage;
  late int lastPage;
  late int perPage;
  late int total;
}

/// Implements in [RepositoryException] the unsuccessful fields
abstract class ArguedFailure {
  late String message;
  late List<FieldError> errors;
}
