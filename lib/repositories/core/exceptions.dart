part of repositories;

class RepositoryException implements Exception {
  const RepositoryException(this.reason);

  final String reason;

  @override
  String toString() {
    return 'Repository Exception: $reason';
  }
}

class UnsuccessfulException extends RepositoryException
    implements ArguedFailure {
  UnsuccessfulException({
    required this.message,
    required this.errors,
  }) : super('Unsuccessful response');

  @override
  String message;

  @override
  List<FieldError> errors;
}

class UnauthorizedException extends RepositoryException {
  const UnauthorizedException() : super('Unauthorized');
}

class ConnectionException extends RepositoryException {
  const ConnectionException() : super('Connection Rejected');
}

class InternalException extends RepositoryException {
  const InternalException([String message = 'Internal Error']) : super(message);
}
