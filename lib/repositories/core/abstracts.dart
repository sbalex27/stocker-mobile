part of repositories;

/// Implements `index` methods for collections
abstract class Pageable<T> {
  /// Index a colletion of the specified `T` resource
  /// passing the `page` argument
  /// 
  /// **Example:**
  /// ```
  /// // Note: Includes meta
  /// Paginated paginated = await service.index(1);
  /// 
  /// // Only data
  /// List data = paginated.data;
  /// ```
  Future<Paginated<T>> index(int page);
}

/// Implements `show` methods for resources
abstract class Showable<T> {
  /// Show an specified `T` resource from the `id` argument
  /// 
  /// **Example:**
  /// ```
  /// var model = await service.show(1);
  /// ```
  Future<T> show(int id);
}

/// Implements the `store` method for resources
abstract class Storable<T> {
  /// Persist a `T` resource in the service
  /// passing the `model` argument
  /// 
  /// **Example:**
  /// ```
  /// var stored = await service.store(model);
  /// ```
  Future<T> store(Serializable model);
}

/// Implements the `update` method for resources
abstract class Editable<T> {
  /// Update an resource of type `T` in the service
  /// passing the `model` argument
  /// 
  /// **Example:**
  /// ```
  /// var updated = await service.update(model);
  /// ```
  Future<T> update(int id, Serializable model);
}

/// Implements the `delete` method for resources
abstract class Removable<T> {
  /// Delete an resource of type `T` in the service
  /// passing the `model` argument
  /// 
  /// **Example:**
  /// ```
  /// var deleted = await service.delete(model);
  /// ```
  Future<T> delete(Identifiable model);
}

/// Collects the inferfaces for the the C.R.U.D. base methods for
/// - Create
/// - Read
/// - Update
/// - Delete
abstract class Crudable<T>
    implements
        Pageable<T>,
        Showable<T>,
        Storable<T>,
        Editable<T>,
        Removable<T> {}
