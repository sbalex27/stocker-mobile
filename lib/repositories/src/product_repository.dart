part of repositories;

abstract class ProductRepository
    implements
        Storable<Product>,
        Removable<Product>,
        Editable<Product>,
        Pageable<Product> {
  /// Show the specified resource by the `id`
  /// if you need load all the detailed info for all the relations
  ///  then set the `detailed` argument on true.
  /// ```
  /// var product = await service.show(1, detailed: loadRelations);
  /// ```
  Future<Product> show(int id, {bool detailed = false});
}
