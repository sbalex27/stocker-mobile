part of repositories;

/// The brands repository
/// 
/// **Implementation**
/// 
/// Extends this abstract class to delegated service
/// ```
/// class BrandsService extends BrandsRepository {
///   // Override implementation
/// }
/// ```
/// 
/// **Provider**
/// 
/// *(Recomended)* Wrap this repository on a
/// Provider, BloC or Controller.
/// ```
/// Provider<BrandsRepository>(
///   create: (_) => BrandsService(),
/// );
/// ```
/// 
abstract class BrandsRepository extends Crudable<Brand> {
  // CRUD inherited methods
}
