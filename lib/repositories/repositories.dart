library repositories;

import 'package:stocker/models/models.dart';

// Repos
part 'src/product_repository.dart';
part 'src/category_repository.dart';
part 'src/brands_repository.dart';

// Core
part 'core/abstracts.dart';
part 'core/exceptions.dart';
part 'core/entities.dart';
